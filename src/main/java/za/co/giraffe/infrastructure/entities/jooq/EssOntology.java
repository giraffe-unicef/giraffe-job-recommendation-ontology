/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq;


import org.jooq.Catalog;
import org.jooq.Sequence;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;
import za.co.giraffe.infrastructure.entities.jooq.tables.*;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.jooq.org",
                "jOOQ version:3.11.9"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class EssOntology extends SchemaImpl {

    /**
     * The reference instance of <code>ess_ontology</code>
     */
    public static final EssOntology ESS_ONTOLOGY = new EssOntology();
    private static final long serialVersionUID = 1421109954;
    /**
     * The table <code>ess_ontology.function</code>.
     */
    public final Function FUNCTION = za.co.giraffe.infrastructure.entities.jooq.tables.Function.FUNCTION;

    /**
     * The table <code>ess_ontology.industry</code>.
     */
    public final Industry INDUSTRY = za.co.giraffe.infrastructure.entities.jooq.tables.Industry.INDUSTRY;

    /**
     * The table <code>ess_ontology.industry_agnostic_products_services</code>.
     */
    public final IndustryAgnosticProductsServices INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES = za.co.giraffe.infrastructure.entities.jooq.tables.IndustryAgnosticProductsServices.INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES;

    /**
     * The table <code>ess_ontology.industry_agnostic_qualification</code>.
     */
    public final IndustryAgnosticQualification INDUSTRY_AGNOSTIC_QUALIFICATION = za.co.giraffe.infrastructure.entities.jooq.tables.IndustryAgnosticQualification.INDUSTRY_AGNOSTIC_QUALIFICATION;

    /**
     * The table <code>ess_ontology.industry_agnostic_skill</code>.
     */
    public final IndustryAgnosticSkill INDUSTRY_AGNOSTIC_SKILL = za.co.giraffe.infrastructure.entities.jooq.tables.IndustryAgnosticSkill.INDUSTRY_AGNOSTIC_SKILL;

    /**
     * The table <code>ess_ontology.industry_and_positions_skill</code>.
     */
    public final IndustryAndPositionsSkill INDUSTRY_AND_POSITIONS_SKILL = za.co.giraffe.infrastructure.entities.jooq.tables.IndustryAndPositionsSkill.INDUSTRY_AND_POSITIONS_SKILL;

    /**
     * The table <code>ess_ontology.industry_and_postions_products_services</code>.
     */
    public final IndustryAndPostionsProductsServices INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES = za.co.giraffe.infrastructure.entities.jooq.tables.IndustryAndPostionsProductsServices.INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES;

    /**
     * The table <code>ess_ontology.industry_and_postions_qualification</code>.
     */
    public final IndustryAndPostionsQualification INDUSTRY_AND_POSTIONS_QUALIFICATION = za.co.giraffe.infrastructure.entities.jooq.tables.IndustryAndPostionsQualification.INDUSTRY_AND_POSTIONS_QUALIFICATION;

    /**
     * The table <code>ess_ontology.industry_position_products_services_group</code>.
     */
    public final IndustryPositionProductsServicesGroup INDUSTRY_POSITION_PRODUCTS_SERVICES_GROUP = za.co.giraffe.infrastructure.entities.jooq.tables.IndustryPositionProductsServicesGroup.INDUSTRY_POSITION_PRODUCTS_SERVICES_GROUP;

    /**
     * The table <code>ess_ontology.industry_position_qualifications_group</code>.
     */
    public final IndustryPositionQualificationsGroup INDUSTRY_POSITION_QUALIFICATIONS_GROUP = za.co.giraffe.infrastructure.entities.jooq.tables.IndustryPositionQualificationsGroup.INDUSTRY_POSITION_QUALIFICATIONS_GROUP;

    /**
     * The table <code>ess_ontology.industry_position_skills_group</code>.
     */
    public final IndustryPositionSkillsGroup INDUSTRY_POSITION_SKILLS_GROUP = za.co.giraffe.infrastructure.entities.jooq.tables.IndustryPositionSkillsGroup.INDUSTRY_POSITION_SKILLS_GROUP;

    /**
     * The table <code>ess_ontology.industrys_sector</code>.
     */
    public final IndustrysSector INDUSTRYS_SECTOR = za.co.giraffe.infrastructure.entities.jooq.tables.IndustrysSector.INDUSTRYS_SECTOR;

    /**
     * The table <code>ess_ontology.ontology_master</code>.
     */
    public final OntologyMaster ONTOLOGY_MASTER = za.co.giraffe.infrastructure.entities.jooq.tables.OntologyMaster.ONTOLOGY_MASTER;

    /**
     * The table <code>ess_ontology.position</code>.
     */
    public final Position POSITION = za.co.giraffe.infrastructure.entities.jooq.tables.Position.POSITION;

    /**
     * The table <code>ess_ontology.position_agnostic_mapping</code>.
     */
    public final PositionAgnosticMapping POSITION_AGNOSTIC_MAPPING = za.co.giraffe.infrastructure.entities.jooq.tables.PositionAgnosticMapping.POSITION_AGNOSTIC_MAPPING;

    /**
     * The table <code>ess_ontology.positions_function</code>.
     */
    public final PositionsFunction POSITIONS_FUNCTION = za.co.giraffe.infrastructure.entities.jooq.tables.PositionsFunction.POSITIONS_FUNCTION;

    /**
     * The table <code>ess_ontology.products_services</code>.
     */
    public final ProductsServices PRODUCTS_SERVICES = za.co.giraffe.infrastructure.entities.jooq.tables.ProductsServices.PRODUCTS_SERVICES;

    /**
     * The table <code>ess_ontology.qualification</code>.
     */
    public final Qualification QUALIFICATION = za.co.giraffe.infrastructure.entities.jooq.tables.Qualification.QUALIFICATION;

    /**
     * The table <code>ess_ontology.sector</code>.
     */
    public final Sector SECTOR = za.co.giraffe.infrastructure.entities.jooq.tables.Sector.SECTOR;

    /**
     * The table <code>ess_ontology.skill</code>.
     */
    public final Skill SKILL = za.co.giraffe.infrastructure.entities.jooq.tables.Skill.SKILL;

    /**
     * No further instances allowed
     */
    private EssOntology() {
        super("ess_ontology", null);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Sequence<?>> getSequences() {
        List result = new ArrayList();
        result.addAll(getSequences0());
        return result;
    }

    private final List<Sequence<?>> getSequences0() {
        return Arrays.<Sequence<?>>asList(
                Sequences.FUNCTION_ID_SEQ,
                Sequences.INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES_ID_SEQ,
                Sequences.INDUSTRY_AGNOSTIC_QUALIFICATION_ID_SEQ,
                Sequences.INDUSTRY_AGNOSTIC_SKILL_ID_SEQ,
                Sequences.INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES_ID_SEQ,
                Sequences.INDUSTRY_AND_POSTIONS_QUALIFICATION_ID_SEQ,
                Sequences.INDUSTRY_ID_SEQ,
                Sequences.INDUSTRYS_SECTOR_ID_SEQ,
                Sequences.POSITION_AGNOSTIC_MAPPING_ID_SEQ,
                Sequences.POSITION_ID_SEQ,
                Sequences.POSITIONS_FUNCTION_ID_SEQ,
                Sequences.POSITIONS_SKILL_ID_SEQ,
                Sequences.PRODUCTS_SERVICES_ID_SEQ,
                Sequences.QUALIFICATION_ID_SEQ,
                Sequences.SKILL_ID_SEQ);
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
                Function.FUNCTION,
                Industry.INDUSTRY,
                IndustryAgnosticProductsServices.INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES,
                IndustryAgnosticQualification.INDUSTRY_AGNOSTIC_QUALIFICATION,
                IndustryAgnosticSkill.INDUSTRY_AGNOSTIC_SKILL,
                IndustryAndPositionsSkill.INDUSTRY_AND_POSITIONS_SKILL,
                IndustryAndPostionsProductsServices.INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES,
                IndustryAndPostionsQualification.INDUSTRY_AND_POSTIONS_QUALIFICATION,
                IndustryPositionProductsServicesGroup.INDUSTRY_POSITION_PRODUCTS_SERVICES_GROUP,
                IndustryPositionQualificationsGroup.INDUSTRY_POSITION_QUALIFICATIONS_GROUP,
                IndustryPositionSkillsGroup.INDUSTRY_POSITION_SKILLS_GROUP,
                IndustrysSector.INDUSTRYS_SECTOR,
                OntologyMaster.ONTOLOGY_MASTER,
                Position.POSITION,
                PositionAgnosticMapping.POSITION_AGNOSTIC_MAPPING,
                PositionsFunction.POSITIONS_FUNCTION,
                ProductsServices.PRODUCTS_SERVICES,
                Qualification.QUALIFICATION,
                Sector.SECTOR,
                Skill.SKILL);
    }
}
