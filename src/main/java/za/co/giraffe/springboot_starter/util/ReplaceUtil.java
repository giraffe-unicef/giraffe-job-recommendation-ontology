package za.co.giraffe.springboot_starter.util;

import java.util.Map;

public class ReplaceUtil {

    public static String replace(String s, Map<String, String> m) {
        for (Map.Entry<String, String> entry : m.entrySet()) {
            s = s.replace(entry.getKey().toString(), entry.getValue().toString());
        }
        return s;
    }
}
