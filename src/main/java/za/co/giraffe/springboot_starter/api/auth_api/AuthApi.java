package za.co.giraffe.springboot_starter.api.auth_api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.co.giraffe.springboot_starter.model.auth.AuthRequest;
import za.co.giraffe.springboot_starter.model.auth.AuthResponse;

import javax.validation.Valid;

@Api(value = "Auth API", tags = {"Auth API"})
public interface AuthApi {
    @ApiOperation(value = "Login user", nickname = "loginUser", notes = "Login user for ops tool", response = Boolean.class, tags = {"Auth API"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Boolean.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "bad request", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/login",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<AuthResponse> loginUser
            (@ApiParam(value = "") @Valid @RequestBody AuthRequest body);
}
