package za.co.giraffe.springboot_starter.util;

public class MobileConverter {
    public static String convertPhoneNumberToExtFormat(String phoneNumber) {
        String removedChars = phoneNumber.replace(" ", "");

        if (removedChars.trim().charAt(0) == '0') {
            return "27" + removedChars.substring(1);
        } else if (removedChars.substring(0, 3).equals("+27")) {
            return "27" + removedChars.substring(3);
        } else {
            return removedChars;
        }
    }

    public static String convertPhoneNumberToZeroFormat(String phoneNumber) throws Exception {
        String removedChars = phoneNumber.replace(" ", "");
        try {
            if (removedChars.substring(0, 2).trim().equals("27")) {
                return "0" + removedChars.substring(2);
            }
            return removedChars;
        } catch (Exception e) {
            throw new Exception("Invalid phone number");
        }
    }

    public static String convertPhoneNumberToZeroFormatWithSpaces(String phoneNumber) {
        try {
            String converted = convertPhoneNumberToZeroFormat(phoneNumber);
            return converted.substring(0, 3) + " " + converted.substring(3, 6) + " " + converted.substring(6);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
