package za.co.giraffe.springboot_starter.api.ontology.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryPositionProductsServicesGroup;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryPositionQualificationsGroup;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryPositionSkillsGroup;
import za.co.giraffe.springboot_starter.api.ontology.OntologyEditorApi;
import za.co.giraffe.springboot_starter.model.ontology.*;
import za.co.giraffe.springboot_starter.model.request.*;
import za.co.giraffe.springboot_starter.services.ontology.IOntologyService;

import javax.validation.Valid;
import java.util.List;

@Controller
public class OntologyEditorApiController implements OntologyEditorApi {
    private static final Logger log = LoggerFactory.getLogger(OntologyEditorApiController.class);
    private IOntologyService iOntologyService;

    @Autowired
    public OntologyEditorApiController(IOntologyService iOntologyService) {
        this.iOntologyService = iOntologyService;
    }

    @Override
    public ResponseEntity<List<PositionWithAgnosticMapping>> getALlPositions(Boolean agnosticIndustry, String section) {
        try {
            return new ResponseEntity<>(iOntologyService.handleFetchAllPosition(agnosticIndustry, section), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    public ResponseEntity<List<ProductsAndServices>> getAllProductsAndServices() {
        try {
            return new ResponseEntity<>(iOntologyService.handleFetchAllProductsAndServices(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<Qualification>> getAllQualifications() {
        try {
            return new ResponseEntity<>(iOntologyService.handleFetchAllQualifications(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<Skill>> getAllSkills() {
        try {
            return new ResponseEntity<>(iOntologyService.handleFetchAllSkills(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<PositionWithAgnosticMapping> createPositions(@Valid PositionRequest body) {
        try {
            return new ResponseEntity<>(iOntologyService.handleCreatePositionWithAgnosticMapping(body), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> createProductsAndServices(@Valid OntologyEditorRequest ontologyEditorRequest) {
        try {
            iOntologyService.handleUpsertProductsAndServices(0, ontologyEditorRequest);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> createQualifications(@Valid OntologyEditorRequest ontologyEditorRequest) {
        try {
            iOntologyService.handleUpsertQualifications(0, ontologyEditorRequest);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> createSkills(@Valid OntologyEditorRequest ontologyEditorRequest) {
        try {
            iOntologyService.handleUpsertSkills(0, ontologyEditorRequest);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> updatePositions(Integer positionId, @Valid OntologyEditorRequest ontologyEditorRequest) {
        try {
            iOntologyService.handleUpdatePosition(positionId, ontologyEditorRequest);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> updateProductsAndServices(Integer productAndServicesId, @Valid OntologyEditorRequest ontologyEditorRequest) {
        try {
            iOntologyService.handleUpsertProductsAndServices(productAndServicesId, ontologyEditorRequest);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> updateQualifications(Integer qualificationsId, @Valid OntologyEditorRequest ontologyEditorRequest) {
        try {
            iOntologyService.handleUpsertQualifications(qualificationsId, ontologyEditorRequest);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> updateSkills(Integer skillsId, @Valid OntologyEditorRequest ontologyEditorRequest) {
        try {
            iOntologyService.handleUpsertSkills(skillsId, ontologyEditorRequest);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<PositionFunctionMapping>> getALlPositionsFunctionMapping() {
        try {
            return new ResponseEntity<>(iOntologyService.handleFetchAllPositionFunctionMapping(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> createPositionsFunctionMapping(UpsertPositionFunctionMapping upsertPositionFunctionMapping) {
        try {
            iOntologyService.handleUpsertPositionFunctionMapping(upsertPositionFunctionMapping);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<IndustryPositionProductsServicesGroup>> getIndustryPositionProductsServicesGroup() {
        try {
            return new ResponseEntity<>(iOntologyService.handleFetchIndustryPositionProductsServicesGroup(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<IndustryPositionQualificationsGroup>> getIndustryPositionQualificationsGroup() {
        try {
            return new ResponseEntity<>(iOntologyService.handleFetchIndustryPositionQualificationsGroup(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<IndustryPositionSkillsGroup>> getIndustryPositionSkillsGroup() {
        try {
            return new ResponseEntity<>(iOntologyService.handleFetchIndustryPositionSkillsGroup(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> updateIndustryPositionProductAndServiceGroupMapping(@Valid UpdateOntologyMapping updateOntologyMapping) {
        try {
            iOntologyService.handleUpsertIndustryPositionProductsServicesGroupMapping(updateOntologyMapping);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> updateIndustryPositionQualificationsGroupMapping(@Valid UpdateOntologyMapping updateOntologyMapping) {
        try {
            iOntologyService.handleUpsertIndustryPositionQualificationsGroupMapping(updateOntologyMapping);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> updateIndustryPositionSkillsGroupMapping(@Valid UpdateOntologyMapping updateOntologyMapping) {
        try {
            iOntologyService.handleUpsertIndustryPositionSkillsGroupMapping(updateOntologyMapping);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> createIndustryPositionProductAndServiceGroupMapping(@Valid CreateOntologyMapping createOntologyMapping) {
        try {
            iOntologyService.handleCreateIndustryPositionProductAndServiceGroupMapping(createOntologyMapping);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> createIndustryPositionQualificationsGroupMapping(@Valid CreateOntologyMapping createOntologyMapping) {
        try {
            iOntologyService.handleCreateIndustryPositionQualificationMapping(createOntologyMapping);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> createIndustryPositionSkillsGroupMapping(@Valid CreateOntologyMapping createOntologyMapping) {
        try {
            iOntologyService.handleCreateIndustryPositionSkillsGroupMapping(createOntologyMapping);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*@Override
    public ResponseEntity<List<OntologyMaster>> getALlOntologyMapping() {
        try {
            return new ResponseEntity<>(iOntologyService.handleFetchAllOntologyMapping(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> updateOntologyMapping(@Valid UpdateOntologyMasterMapping updateOntologyMasterMapping) {
        try {
            iOntologyService.handleUpsertOntologyMaster(updateOntologyMasterMapping);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }*/
}
