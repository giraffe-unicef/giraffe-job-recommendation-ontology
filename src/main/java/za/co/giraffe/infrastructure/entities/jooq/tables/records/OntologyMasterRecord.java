/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables.records;


import org.jooq.Field;
import org.jooq.Record10;
import org.jooq.Row10;
import org.jooq.impl.TableRecordImpl;
import za.co.giraffe.infrastructure.entities.jooq.tables.OntologyMaster;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.jooq.org",
                "jOOQ version:3.11.9"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class OntologyMasterRecord extends TableRecordImpl<OntologyMasterRecord> implements Record10<Integer, String, Integer, String, Boolean, Boolean, Boolean, Integer[], Integer[], Integer[]> {

    private static final long serialVersionUID = -289316188;

    /**
     * Create a detached OntologyMasterRecord
     */
    public OntologyMasterRecord() {
        super(OntologyMaster.ONTOLOGY_MASTER);
    }

    /**
     * Create a detached, initialised OntologyMasterRecord
     */
    public OntologyMasterRecord(Integer id, String positionName, Integer industryId, String industryName, Boolean agnosticProductsServices, Boolean agnosticQualification, Boolean agnosticSkill, Integer[] skillIds, Integer[] productServiceIds, Integer[] qualificationIds) {
        super(OntologyMaster.ONTOLOGY_MASTER);

        set(0, id);
        set(1, positionName);
        set(2, industryId);
        set(3, industryName);
        set(4, agnosticProductsServices);
        set(5, agnosticQualification);
        set(6, agnosticSkill);
        set(7, skillIds);
        set(8, productServiceIds);
        set(9, qualificationIds);
    }

    /**
     * Getter for <code>ess_ontology.ontology_master.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>ess_ontology.ontology_master.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>ess_ontology.ontology_master.position_name</code>.
     */
    public String getPositionName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>ess_ontology.ontology_master.position_name</code>.
     */
    public void setPositionName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>ess_ontology.ontology_master.industry_id</code>.
     */
    public Integer getIndustryId() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>ess_ontology.ontology_master.industry_id</code>.
     */
    public void setIndustryId(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>ess_ontology.ontology_master.industry_name</code>.
     */
    public String getIndustryName() {
        return (String) get(3);
    }

    /**
     * Setter for <code>ess_ontology.ontology_master.industry_name</code>.
     */
    public void setIndustryName(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>ess_ontology.ontology_master.agnostic_products_services</code>.
     */
    public Boolean getAgnosticProductsServices() {
        return (Boolean) get(4);
    }

    /**
     * Setter for <code>ess_ontology.ontology_master.agnostic_products_services</code>.
     */
    public void setAgnosticProductsServices(Boolean value) {
        set(4, value);
    }

    /**
     * Getter for <code>ess_ontology.ontology_master.agnostic_qualification</code>.
     */
    public Boolean getAgnosticQualification() {
        return (Boolean) get(5);
    }

    /**
     * Setter for <code>ess_ontology.ontology_master.agnostic_qualification</code>.
     */
    public void setAgnosticQualification(Boolean value) {
        set(5, value);
    }

    /**
     * Getter for <code>ess_ontology.ontology_master.agnostic_skill</code>.
     */
    public Boolean getAgnosticSkill() {
        return (Boolean) get(6);
    }

    /**
     * Setter for <code>ess_ontology.ontology_master.agnostic_skill</code>.
     */
    public void setAgnosticSkill(Boolean value) {
        set(6, value);
    }

    /**
     * Getter for <code>ess_ontology.ontology_master.skill_ids</code>.
     */
    public Integer[] getSkillIds() {
        return (Integer[]) get(7);
    }

    /**
     * Setter for <code>ess_ontology.ontology_master.skill_ids</code>.
     */
    public void setSkillIds(Integer... value) {
        set(7, value);
    }

    /**
     * Getter for <code>ess_ontology.ontology_master.product_service_ids</code>.
     */
    public Integer[] getProductServiceIds() {
        return (Integer[]) get(8);
    }

    /**
     * Setter for <code>ess_ontology.ontology_master.product_service_ids</code>.
     */
    public void setProductServiceIds(Integer... value) {
        set(8, value);
    }

    // -------------------------------------------------------------------------
    // Record10 type implementation
    // -------------------------------------------------------------------------

    /**
     * Getter for <code>ess_ontology.ontology_master.qualification_ids</code>.
     */
    public Integer[] getQualificationIds() {
        return (Integer[]) get(9);
    }

    /**
     * Setter for <code>ess_ontology.ontology_master.qualification_ids</code>.
     */
    public void setQualificationIds(Integer... value) {
        set(9, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row10<Integer, String, Integer, String, Boolean, Boolean, Boolean, Integer[], Integer[], Integer[]> fieldsRow() {
        return (Row10) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row10<Integer, String, Integer, String, Boolean, Boolean, Boolean, Integer[], Integer[], Integer[]> valuesRow() {
        return (Row10) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return OntologyMaster.ONTOLOGY_MASTER.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return OntologyMaster.ONTOLOGY_MASTER.POSITION_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return OntologyMaster.ONTOLOGY_MASTER.INDUSTRY_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return OntologyMaster.ONTOLOGY_MASTER.INDUSTRY_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Boolean> field5() {
        return OntologyMaster.ONTOLOGY_MASTER.AGNOSTIC_PRODUCTS_SERVICES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Boolean> field6() {
        return OntologyMaster.ONTOLOGY_MASTER.AGNOSTIC_QUALIFICATION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Boolean> field7() {
        return OntologyMaster.ONTOLOGY_MASTER.AGNOSTIC_SKILL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer[]> field8() {
        return OntologyMaster.ONTOLOGY_MASTER.SKILL_IDS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer[]> field9() {
        return OntologyMaster.ONTOLOGY_MASTER.PRODUCT_SERVICE_IDS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer[]> field10() {
        return OntologyMaster.ONTOLOGY_MASTER.QUALIFICATION_IDS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getPositionName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component3() {
        return getIndustryId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getIndustryName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean component5() {
        return getAgnosticProductsServices();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean component6() {
        return getAgnosticQualification();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean component7() {
        return getAgnosticSkill();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] component8() {
        return getSkillIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] component9() {
        return getProductServiceIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] component10() {
        return getQualificationIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getPositionName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getIndustryId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getIndustryName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean value5() {
        return getAgnosticProductsServices();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean value6() {
        return getAgnosticQualification();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean value7() {
        return getAgnosticSkill();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] value8() {
        return getSkillIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] value9() {
        return getProductServiceIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] value10() {
        return getQualificationIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OntologyMasterRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OntologyMasterRecord value2(String value) {
        setPositionName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OntologyMasterRecord value3(Integer value) {
        setIndustryId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OntologyMasterRecord value4(String value) {
        setIndustryName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OntologyMasterRecord value5(Boolean value) {
        setAgnosticProductsServices(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OntologyMasterRecord value6(Boolean value) {
        setAgnosticQualification(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OntologyMasterRecord value7(Boolean value) {
        setAgnosticSkill(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OntologyMasterRecord value8(Integer... value) {
        setSkillIds(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OntologyMasterRecord value9(Integer... value) {
        setProductServiceIds(value);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public OntologyMasterRecord value10(Integer... value) {
        setQualificationIds(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OntologyMasterRecord values(Integer value1, String value2, Integer value3, String value4, Boolean value5, Boolean value6, Boolean value7, Integer[] value8, Integer[] value9, Integer[] value10) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        return this;
    }
}
