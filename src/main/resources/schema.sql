/*
 Navicat PostgreSQL Data Transfer

 Source Server         : localhost
 Source Server Type    : PostgreSQL
 Source Server Version : 100015
 Source Host           : localhost:5432
 Source Catalog        : giraffe
 Source Schema         : ess_ontology

 Target Server Type    : PostgreSQL
 Target Server Version : 100015
 File Encoding         : 65001

 Date: 28/04/2021 19:37:46
*/


-- ----------------------------
-- Sequence structure for function_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."function_id_seq";
CREATE SEQUENCE "ess_ontology"."function_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for industry_agnostic_products_services_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."industry_agnostic_products_services_id_seq";
CREATE SEQUENCE "ess_ontology"."industry_agnostic_products_services_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for industry_agnostic_qualification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."industry_agnostic_qualification_id_seq";
CREATE SEQUENCE "ess_ontology"."industry_agnostic_qualification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 112
CACHE 1;

-- ----------------------------
-- Sequence structure for industry_agnostic_skill_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."industry_agnostic_skill_id_seq";
CREATE SEQUENCE "ess_ontology"."industry_agnostic_skill_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 103
CACHE 1;

-- ----------------------------
-- Sequence structure for industry_and_postions_products_services_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."industry_and_postions_products_services_id_seq";
CREATE SEQUENCE "ess_ontology"."industry_and_postions_products_services_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for industry_and_postions_qualification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."industry_and_postions_qualification_id_seq";
CREATE SEQUENCE "ess_ontology"."industry_and_postions_qualification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for industry_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."industry_id_seq";
CREATE SEQUENCE "ess_ontology"."industry_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for industrys_sector_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."industrys_sector_id_seq";
CREATE SEQUENCE "ess_ontology"."industrys_sector_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for position_agnostic_mapping_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."position_agnostic_mapping_id_seq";
CREATE SEQUENCE "ess_ontology"."position_agnostic_mapping_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for position_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."position_id_seq";
CREATE SEQUENCE "ess_ontology"."position_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for positions_function_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."positions_function_id_seq";
CREATE SEQUENCE "ess_ontology"."positions_function_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for positions_skill_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."positions_skill_id_seq";
CREATE SEQUENCE "ess_ontology"."positions_skill_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for products_services_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."products_services_id_seq";
CREATE SEQUENCE "ess_ontology"."products_services_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for qualification_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."qualification_id_seq";
CREATE SEQUENCE "ess_ontology"."qualification_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for skill_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "ess_ontology"."skill_id_seq";
CREATE SEQUENCE "ess_ontology"."skill_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for function
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."function";
CREATE TABLE "ess_ontology"."function" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".function_id_seq'::regclass),
  "description" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for industry
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."industry";
CREATE TABLE "ess_ontology"."industry" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".industry_id_seq'::regclass),
  "description" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for products_services
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."products_services";
CREATE TABLE "ess_ontology"."products_services" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".products_services_id_seq'::regclass),
  "name" text COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for qualification
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."qualification";
CREATE TABLE "ess_ontology"."qualification" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".qualification_id_seq'::regclass),
  "name" text COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for sector
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."sector";
CREATE TABLE "ess_ontology"."sector" (
  "id" uuid NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "is_industry" bool NOT NULL
)
;

-- ----------------------------
-- Table structure for skill
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."skill";
CREATE TABLE "ess_ontology"."skill" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".skill_id_seq'::regclass),
  "name" text COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for position
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."position";
CREATE TABLE "ess_ontology"."position" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".position_id_seq'::regclass),
  "description" text COLLATE "pg_catalog"."default"
)
;


-- ----------------------------
-- Table structure for industry_agnostic_products_services
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."industry_agnostic_products_services";
CREATE TABLE "ess_ontology"."industry_agnostic_products_services" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".industry_agnostic_products_services_id_seq'::regclass),
  "position_id" int4 NOT NULL,
  "product_service_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for industry_agnostic_qualification
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."industry_agnostic_qualification";
CREATE TABLE "ess_ontology"."industry_agnostic_qualification" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".industry_agnostic_qualification_id_seq'::regclass),
  "position_id" int4 NOT NULL,
  "qualification_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for industry_agnostic_skill
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."industry_agnostic_skill";
CREATE TABLE "ess_ontology"."industry_agnostic_skill" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".industry_agnostic_skill_id_seq'::regclass),
  "position_id" int4 NOT NULL,
  "skill_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for industry_and_positions_skill
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."industry_and_positions_skill";
CREATE TABLE "ess_ontology"."industry_and_positions_skill" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".positions_skill_id_seq'::regclass),
  "industry_id" int4 NOT NULL,
  "position_id" int4 NOT NULL,
  "skill_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for industry_and_postions_products_services
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."industry_and_postions_products_services";
CREATE TABLE "ess_ontology"."industry_and_postions_products_services" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".industry_and_postions_products_services_id_seq'::regclass),
  "industry_id" int4 NOT NULL,
  "position_id" int4 NOT NULL,
  "product_service_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for industry_and_postions_qualification
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."industry_and_postions_qualification";
CREATE TABLE "ess_ontology"."industry_and_postions_qualification" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".industry_and_postions_qualification_id_seq'::regclass),
  "industry_id" int4 NOT NULL,
  "position_id" int4 NOT NULL,
  "qualification_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for industrys_sector
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."industrys_sector";
CREATE TABLE "ess_ontology"."industrys_sector" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".industrys_sector_id_seq'::regclass),
  "industry_id" int4 NOT NULL,
  "sector_id" uuid NOT NULL
)
;


-- ----------------------------
-- Table structure for position_agnostic_mapping
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."position_agnostic_mapping";
CREATE TABLE "ess_ontology"."position_agnostic_mapping" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".position_agnostic_mapping_id_seq'::regclass),
  "position_id" int4,
  "agnostic_products_services" bool,
  "agnostic_qualification" bool,
  "agnostic_skill" bool
)
;

-- ----------------------------
-- Table structure for positions_function
-- ----------------------------
DROP TABLE IF EXISTS "ess_ontology"."positions_function";
CREATE TABLE "ess_ontology"."positions_function" (
  "id" int4 NOT NULL DEFAULT nextval('"ess_ontology".positions_function_id_seq'::regclass),
  "position_id" int4 NOT NULL,
  "function_id" int4 NOT NULL
)
;



-- ----------------------------
-- View structure for industry_position_products_services_group
-- ----------------------------
DROP VIEW IF EXISTS "ess_ontology"."industry_position_products_services_group";
CREATE VIEW "ess_ontology"."industry_position_products_services_group" AS  SELECT array_agg(DISTINCT test.position_id) AS position_ids,
    array_agg(DISTINCT test.industry_id) AS industry_ids,
    test.product_service_id AS product_service_ids,
    test.agnostic_products_services
   FROM ( SELECT "position".id AS position_id,
            industry_and_postions_products_services.industry_id,
            array_agg(
                CASE
                    WHEN industry_and_postions_products_services.product_service_id IS NOT NULL THEN industry_and_postions_products_services.product_service_id
                    WHEN industry_agnostic_products_services.product_service_id IS NOT NULL THEN industry_agnostic_products_services.product_service_id
                    ELSE NULL::integer
                END) AS product_service_id,
            position_agnostic_mapping.agnostic_products_services
           FROM ess_ontology."position"
             LEFT JOIN ess_ontology.industry_agnostic_products_services ON industry_agnostic_products_services.position_id = "position".id
             LEFT JOIN ess_ontology.industry_and_postions_products_services ON industry_and_postions_products_services.position_id = "position".id
             LEFT JOIN ess_ontology.position_agnostic_mapping ON position_agnostic_mapping.position_id = "position".id
          GROUP BY "position".id, industry_and_postions_products_services.industry_id, position_agnostic_mapping.agnostic_products_services) test
  GROUP BY test.product_service_id, test.agnostic_products_services;

-- ----------------------------
-- View structure for industry_position_skills_group
-- ----------------------------
DROP VIEW IF EXISTS "ess_ontology"."industry_position_skills_group";
CREATE VIEW "ess_ontology"."industry_position_skills_group" AS  SELECT array_agg(DISTINCT test.position_id ORDER BY test.position_id) AS position_ids,
    array_agg(DISTINCT test.industry_id ORDER BY test.industry_id) AS industry_ids,
    test.skill_ids,
    test.agnostic_skill
   FROM ( SELECT "position".id AS position_id,
            industry_and_positions_skill.industry_id,
            array_agg(
                CASE
                    WHEN industry_and_positions_skill.skill_id IS NOT NULL THEN industry_and_positions_skill.skill_id
                    WHEN industry_agnostic_skill.skill_id IS NOT NULL THEN industry_agnostic_skill.skill_id
                    ELSE NULL::integer
                END ORDER BY industry_and_positions_skill.skill_id, industry_agnostic_skill.skill_id) AS skill_ids,
            position_agnostic_mapping.agnostic_skill
           FROM ess_ontology."position"
             LEFT JOIN ess_ontology.industry_agnostic_skill ON industry_agnostic_skill.position_id = "position".id
             LEFT JOIN ess_ontology.industry_and_positions_skill ON industry_and_positions_skill.position_id = "position".id
             LEFT JOIN ess_ontology.position_agnostic_mapping ON position_agnostic_mapping.position_id = "position".id
          GROUP BY "position".id, industry_and_positions_skill.industry_id, position_agnostic_mapping.agnostic_skill) test
  GROUP BY test.skill_ids, test.agnostic_skill;

-- ----------------------------
-- View structure for industry_position_qualifications_group
-- ----------------------------
DROP VIEW IF EXISTS "ess_ontology"."industry_position_qualifications_group";
CREATE VIEW "ess_ontology"."industry_position_qualifications_group" AS  SELECT array_agg(DISTINCT test.position_id ORDER BY test.position_id) AS position_ids,
    array_agg(DISTINCT test.industry_id ORDER BY test.industry_id) AS industry_ids,
    test.qualification_ids,
    test.agnostic_qualification
   FROM ( SELECT "position".id AS position_id,
            industry_and_postions_qualification.industry_id,
            array_agg(
                CASE
                    WHEN industry_and_postions_qualification.qualification_id IS NOT NULL THEN industry_and_postions_qualification.qualification_id
                    WHEN industry_agnostic_qualification.qualification_id IS NOT NULL THEN industry_agnostic_qualification.qualification_id
                    ELSE NULL::integer
                END ORDER BY industry_and_postions_qualification.qualification_id, industry_agnostic_qualification.qualification_id) AS qualification_ids,
            position_agnostic_mapping.agnostic_qualification
           FROM ess_ontology."position"
             LEFT JOIN ess_ontology.industry_agnostic_qualification ON industry_agnostic_qualification.position_id = "position".id
             LEFT JOIN ess_ontology.industry_and_postions_qualification ON industry_and_postions_qualification.position_id = "position".id
             LEFT JOIN ess_ontology.position_agnostic_mapping ON position_agnostic_mapping.position_id = "position".id
          GROUP BY "position".id, industry_and_postions_qualification.industry_id, position_agnostic_mapping.agnostic_qualification) test
  GROUP BY test.qualification_ids, test.agnostic_qualification;

-- ----------------------------
-- View structure for ontology_master
-- ----------------------------
DROP VIEW IF EXISTS "ess_ontology"."ontology_master";
CREATE VIEW "ess_ontology"."ontology_master" AS  SELECT "position".id,
    "position".description AS position_name,
    skill_mapping.industry_id,
    industry.description AS industry_name,
    position_agnostic_mapping.agnostic_products_services,
    position_agnostic_mapping.agnostic_qualification,
    position_agnostic_mapping.agnostic_skill,
    skill_mapping.skill_id AS skill_ids,
    products_services_mapping.product_service_id AS product_service_ids,
    qualification_mapping.qualification_id AS qualification_ids
   FROM ess_ontology."position"
     LEFT JOIN ( SELECT position_1.id AS position_id,
            industry_and_positions_skill.industry_id,
            array_agg(
                CASE
                    WHEN industry_and_positions_skill.skill_id IS NOT NULL THEN industry_and_positions_skill.skill_id
                    WHEN industry_agnostic_skill.skill_id IS NOT NULL THEN industry_agnostic_skill.skill_id
                    ELSE NULL::integer
                END) AS skill_id
           FROM ess_ontology."position" position_1
             LEFT JOIN ess_ontology.industry_agnostic_skill ON industry_agnostic_skill.position_id = position_1.id
             LEFT JOIN ess_ontology.industry_and_positions_skill ON industry_and_positions_skill.position_id = position_1.id
          GROUP BY position_1.id, industry_and_positions_skill.industry_id) skill_mapping ON skill_mapping.position_id = "position".id
     LEFT JOIN ( SELECT position_1.id AS position_id,
            industry_and_postions_products_services.industry_id,
            array_agg(
                CASE
                    WHEN industry_and_postions_products_services.product_service_id IS NOT NULL THEN industry_and_postions_products_services.product_service_id
                    WHEN industry_agnostic_products_services.product_service_id IS NOT NULL THEN industry_agnostic_products_services.product_service_id
                    ELSE NULL::integer
                END) AS product_service_id
           FROM ess_ontology."position" position_1
             LEFT JOIN ess_ontology.industry_agnostic_products_services ON industry_agnostic_products_services.position_id = position_1.id
             LEFT JOIN ess_ontology.industry_and_postions_products_services ON industry_and_postions_products_services.position_id = position_1.id
          GROUP BY position_1.id, industry_and_postions_products_services.industry_id) products_services_mapping ON products_services_mapping.position_id = skill_mapping.position_id AND products_services_mapping.industry_id = skill_mapping.industry_id
     LEFT JOIN ( SELECT position_1.id AS position_id,
            industry_and_postions_qualification.industry_id,
            array_agg(
                CASE
                    WHEN industry_and_postions_qualification.qualification_id IS NOT NULL THEN industry_and_postions_qualification.qualification_id
                    WHEN industry_agnostic_qualification.qualification_id IS NOT NULL THEN industry_agnostic_qualification.qualification_id
                    ELSE NULL::integer
                END) AS qualification_id
           FROM ess_ontology."position" position_1
             LEFT JOIN ess_ontology.industry_agnostic_qualification ON industry_agnostic_qualification.position_id = position_1.id
             LEFT JOIN ess_ontology.industry_and_postions_qualification ON industry_and_postions_qualification.position_id = position_1.id
          GROUP BY position_1.id, industry_and_postions_qualification.industry_id) qualification_mapping ON qualification_mapping.position_id = skill_mapping.position_id AND qualification_mapping.industry_id = skill_mapping.industry_id
     LEFT JOIN ess_ontology.position_agnostic_mapping ON position_agnostic_mapping.position_id = "position".id
     LEFT JOIN ess_ontology.industry ON skill_mapping.industry_id = industry.id;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."function_id_seq"
OWNED BY "ess_ontology"."function"."id";
SELECT setval('"ess_ontology"."function_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."industry_agnostic_products_services_id_seq"
OWNED BY "ess_ontology"."industry_agnostic_products_services"."id";
SELECT setval('"ess_ontology"."industry_agnostic_products_services_id_seq"', 137, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."industry_agnostic_qualification_id_seq"
OWNED BY "ess_ontology"."industry_agnostic_qualification"."id";
SELECT setval('"ess_ontology"."industry_agnostic_qualification_id_seq"', 171, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."industry_agnostic_skill_id_seq"
OWNED BY "ess_ontology"."industry_agnostic_skill"."id";
SELECT setval('"ess_ontology"."industry_agnostic_skill_id_seq"', 113, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."industry_and_postions_products_services_id_seq"
OWNED BY "ess_ontology"."industry_and_postions_products_services"."id";
SELECT setval('"ess_ontology"."industry_and_postions_products_services_id_seq"', 1045, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."industry_and_postions_qualification_id_seq"
OWNED BY "ess_ontology"."industry_and_postions_qualification"."id";
SELECT setval('"ess_ontology"."industry_and_postions_qualification_id_seq"', 335, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."industry_id_seq"
OWNED BY "ess_ontology"."industry"."id";
SELECT setval('"ess_ontology"."industry_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."industrys_sector_id_seq"
OWNED BY "ess_ontology"."industrys_sector"."id";
SELECT setval('"ess_ontology"."industrys_sector_id_seq"', 29, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."position_agnostic_mapping_id_seq"
OWNED BY "ess_ontology"."position_agnostic_mapping"."id";
SELECT setval('"ess_ontology"."position_agnostic_mapping_id_seq"', 169, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."position_id_seq"
OWNED BY "ess_ontology"."position"."id";
SELECT setval('"ess_ontology"."position_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."positions_function_id_seq"
OWNED BY "ess_ontology"."positions_function"."id";
SELECT setval('"ess_ontology"."positions_function_id_seq"', 214, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."positions_skill_id_seq"
OWNED BY "ess_ontology"."industry_and_positions_skill"."id";
SELECT setval('"ess_ontology"."positions_skill_id_seq"', 426, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."products_services_id_seq"
OWNED BY "ess_ontology"."products_services"."id";
SELECT setval('"ess_ontology"."products_services_id_seq"', 63, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."qualification_id_seq"
OWNED BY "ess_ontology"."qualification"."id";
SELECT setval('"ess_ontology"."qualification_id_seq"', 43, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "ess_ontology"."skill_id_seq"
OWNED BY "ess_ontology"."skill"."id";
SELECT setval('"ess_ontology"."skill_id_seq"', 83, true);

-- ----------------------------
-- Primary Key structure for table function
-- ----------------------------
ALTER TABLE "ess_ontology"."function" ADD CONSTRAINT "category_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table industry
-- ----------------------------
ALTER TABLE "ess_ontology"."industry" ADD CONSTRAINT "industry_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table industry_agnostic_products_services
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_agnostic_products_services" ADD CONSTRAINT "industry_agnostic_products_services_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table industry_agnostic_qualification
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_agnostic_qualification" ADD CONSTRAINT "industry_agnostic_qualification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table industry_agnostic_skill
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_agnostic_skill" ADD CONSTRAINT "industry_agnostic_skill_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table industry_and_positions_skill
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_and_positions_skill" ADD CONSTRAINT "positions_skill_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table industry_and_postions_products_services
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_and_postions_products_services" ADD CONSTRAINT "sub_industry_and_postions_products_services_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table industry_and_postions_qualification
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_and_postions_qualification" ADD CONSTRAINT "sub_industry_and_postions_qualification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table industrys_sector
-- ----------------------------
ALTER TABLE "ess_ontology"."industrys_sector" ADD CONSTRAINT "sub_industrys_industry_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table position
-- ----------------------------
ALTER TABLE "ess_ontology"."position" ADD CONSTRAINT "position_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table position_agnostic_mapping
-- ----------------------------
ALTER TABLE "ess_ontology"."position_agnostic_mapping" ADD CONSTRAINT "position_agnostic_mapping_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table positions_function
-- ----------------------------
ALTER TABLE "ess_ontology"."positions_function" ADD CONSTRAINT "positions_function_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table products_services
-- ----------------------------
ALTER TABLE "ess_ontology"."products_services" ADD CONSTRAINT "products_services_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table qualification
-- ----------------------------
ALTER TABLE "ess_ontology"."qualification" ADD CONSTRAINT "qualification_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sector
-- ----------------------------
ALTER TABLE "ess_ontology"."sector" ADD CONSTRAINT "sector_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table skill
-- ----------------------------
ALTER TABLE "ess_ontology"."skill" ADD CONSTRAINT "skill_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table industry_agnostic_products_services
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_agnostic_products_services" ADD CONSTRAINT "position_id" FOREIGN KEY ("position_id") REFERENCES "ess_ontology"."position" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ess_ontology"."industry_agnostic_products_services" ADD CONSTRAINT "product_service_id" FOREIGN KEY ("product_service_id") REFERENCES "ess_ontology"."products_services" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table industry_agnostic_qualification
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_agnostic_qualification" ADD CONSTRAINT "position_id" FOREIGN KEY ("position_id") REFERENCES "ess_ontology"."position" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ess_ontology"."industry_agnostic_qualification" ADD CONSTRAINT "qualification_id" FOREIGN KEY ("qualification_id") REFERENCES "ess_ontology"."qualification" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table industry_agnostic_skill
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_agnostic_skill" ADD CONSTRAINT "position_id" FOREIGN KEY ("position_id") REFERENCES "ess_ontology"."position" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ess_ontology"."industry_agnostic_skill" ADD CONSTRAINT "skill_id" FOREIGN KEY ("skill_id") REFERENCES "ess_ontology"."skill" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table industry_and_positions_skill
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_and_positions_skill" ADD CONSTRAINT "industry_id" FOREIGN KEY ("industry_id") REFERENCES "ess_ontology"."industry" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ess_ontology"."industry_and_positions_skill" ADD CONSTRAINT "position_id" FOREIGN KEY ("position_id") REFERENCES "ess_ontology"."position" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ess_ontology"."industry_and_positions_skill" ADD CONSTRAINT "skill_id" FOREIGN KEY ("skill_id") REFERENCES "ess_ontology"."skill" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table industry_and_postions_products_services
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_and_postions_products_services" ADD CONSTRAINT "industry_id" FOREIGN KEY ("industry_id") REFERENCES "ess_ontology"."industry" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ess_ontology"."industry_and_postions_products_services" ADD CONSTRAINT "position_id" FOREIGN KEY ("position_id") REFERENCES "ess_ontology"."position" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ess_ontology"."industry_and_postions_products_services" ADD CONSTRAINT "product_service_id" FOREIGN KEY ("product_service_id") REFERENCES "ess_ontology"."products_services" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table industry_and_postions_qualification
-- ----------------------------
ALTER TABLE "ess_ontology"."industry_and_postions_qualification" ADD CONSTRAINT "industry_id" FOREIGN KEY ("industry_id") REFERENCES "ess_ontology"."industry" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ess_ontology"."industry_and_postions_qualification" ADD CONSTRAINT "position_id" FOREIGN KEY ("position_id") REFERENCES "ess_ontology"."position" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ess_ontology"."industry_and_postions_qualification" ADD CONSTRAINT "qualification_id" FOREIGN KEY ("qualification_id") REFERENCES "ess_ontology"."qualification" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table industrys_sector
-- ----------------------------
ALTER TABLE "ess_ontology"."industrys_sector" ADD CONSTRAINT "industry_id" FOREIGN KEY ("industry_id") REFERENCES "ess_ontology"."industry" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ess_ontology"."industrys_sector" ADD CONSTRAINT "sector_id" FOREIGN KEY ("sector_id") REFERENCES "ess_ontology"."sector" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table position_agnostic_mapping
-- ----------------------------
ALTER TABLE "ess_ontology"."position_agnostic_mapping" ADD CONSTRAINT "position_agnostic_mapping_position_id_fkey" FOREIGN KEY ("position_id") REFERENCES "ess_ontology"."position" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table positions_function
-- ----------------------------
ALTER TABLE "ess_ontology"."positions_function" ADD CONSTRAINT "function_id" FOREIGN KEY ("function_id") REFERENCES "ess_ontology"."function" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ess_ontology"."positions_function" ADD CONSTRAINT "position_id" FOREIGN KEY ("position_id") REFERENCES "ess_ontology"."position" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
