package za.co.giraffe.springboot_starter;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import za.co.giraffe.springboot_starter.configuration.ProdConfig;
import za.co.giraffe.springboot_starter.configuration.auth.AuthConfig;

@SpringBootApplication
@EnableSwagger2
@EnableConfigurationProperties({ProdConfig.class, AuthConfig.class})
@ComponentScan(basePackages = {"za.co.giraffe", "za.co.giraffe.springboot_starter", "za.co.giraffe.springboot_starter.configuration"})
public class Swagger2SpringBoot implements CommandLineRunner {

    public static void main(String[] args) throws Exception {
        new SpringApplication(Swagger2SpringBoot.class).run(args);
    }

    @Override
    public void run(String... arg0) throws Exception {
        if (arg0.length > 0 && arg0[0].equals("exitcode")) {
            throw new ExitException();
        }
    }

    class ExitException extends RuntimeException implements ExitCodeGenerator {
        private static final long serialVersionUID = 1L;

        @Override
        public int getExitCode() {
            return 10;
        }

    }
}