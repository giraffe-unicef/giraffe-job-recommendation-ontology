package za.co.giraffe.springboot_starter.data.repository.ontology.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.TableField;
import org.sfm.jdbc.JdbcMapper;
import org.sfm.jdbc.JdbcMapperFactory;
import org.sfm.reflect.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.*;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.*;
import za.co.giraffe.springboot_starter.data.repository.ontology.IOntologyDAO;
import za.co.giraffe.springboot_starter.model.ess.Function;
import za.co.giraffe.springboot_starter.model.ontology.Qualification;
import za.co.giraffe.springboot_starter.model.ontology.Skill;
import za.co.giraffe.springboot_starter.model.ontology.*;

import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static org.jooq.impl.DSL.trueCondition;
import static za.co.giraffe.infrastructure.entities.jooq.Tables.*;

@Service
@Transactional("transactionManager")
public class OntologyDAOImpl implements IOntologyDAO {
    private DSLContext dsl;

    @Autowired
    public OntologyDAOImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public List<Function> fetchFunctions() {
        List<Function> records = null;
        try {
            records = dsl.select().from(FUNCTION).orderBy(FUNCTION.ID).fetchInto(Function.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return records;
    }

    @Override
    public List<Position> fetchPositionsByFunctionId(Integer id) {
        List<Position> records = null;
        try {
            records = dsl.select(POSITION.fields()).from(POSITION)
                    .join(POSITIONS_FUNCTION)
                    .on(POSITIONS_FUNCTION.POSITION_ID.eq(POSITION.ID))
                    .join(FUNCTION)
                    .on(POSITIONS_FUNCTION.FUNCTION_ID.eq(FUNCTION.ID))
                    .where(FUNCTION.ID.eq(id)).fetchInto(Position.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return records;
    }

    @Override
    public List<ProductsAndServices> fetchProductsServicesByIndustryPositionIds(Integer industryId, List<Integer> positionIds) {
        List<ProductsAndServices> productsAndServices = null;
        try {
            productsAndServices = dsl.select(PRODUCTS_SERVICES.fields()).from(PRODUCTS_SERVICES)
                    .join(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES)
                    .on(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES.PRODUCT_SERVICE_ID.eq(PRODUCTS_SERVICES.ID))
                    .where(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES.POSITION_ID.in(CollectionUtils.emptyIfNull(positionIds)))
                    .and(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES.INDUSTRY_ID.eq(industryId))
                    .orderBy(PRODUCTS_SERVICES.ID.asc())
                    .fetchInto(ProductsAndServices.class);
            List<ProductsAndServices> agnosticProductsAndServices = dsl.select(PRODUCTS_SERVICES.fields()).from(PRODUCTS_SERVICES)
                    .join(INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES)
                    .on(INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES.PRODUCT_SERVICE_ID.eq(PRODUCTS_SERVICES.ID))
                    .where(INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES.POSITION_ID.in(CollectionUtils.emptyIfNull(positionIds)))
                    .orderBy(PRODUCTS_SERVICES.ID.asc())
                    .fetchInto(ProductsAndServices.class);
            productsAndServices.addAll(agnosticProductsAndServices);
            HashSet<Object> seen = new HashSet<>();
            productsAndServices.removeIf(e -> !seen.add(e.getId()));
            Collections.sort(productsAndServices);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return productsAndServices;
    }

    @Override
    public List<Qualification> fetchQualificationsByIndustryPositionIds(Integer industryId, List<Integer> positionIds) {
        List<Qualification> records = null;
        try {
            records = dsl.select(QUALIFICATION.fields()).from(QUALIFICATION)
                    .join(INDUSTRY_AND_POSTIONS_QUALIFICATION)
                    .on(INDUSTRY_AND_POSTIONS_QUALIFICATION.QUALIFICATION_ID.eq(QUALIFICATION.ID))
                    .where(INDUSTRY_AND_POSTIONS_QUALIFICATION.POSITION_ID.in(CollectionUtils.emptyIfNull(positionIds)))
                    .and(INDUSTRY_AND_POSTIONS_QUALIFICATION.INDUSTRY_ID.eq(industryId))
                    .orderBy(QUALIFICATION.ID.asc())
                    .fetchInto(Qualification.class);
            List<Qualification> agnosticQualifications = dsl.select(QUALIFICATION.fields()).from(QUALIFICATION)
                    .join(INDUSTRY_AGNOSTIC_QUALIFICATION)
                    .on(INDUSTRY_AGNOSTIC_QUALIFICATION.QUALIFICATION_ID.eq(QUALIFICATION.ID))
                    .where(INDUSTRY_AGNOSTIC_QUALIFICATION.POSITION_ID.in(CollectionUtils.emptyIfNull(positionIds)))
                    .orderBy(QUALIFICATION.ID.asc())
                    .fetchInto(Qualification.class);
            records.addAll(agnosticQualifications);
            HashSet<Object> seen = new HashSet<>();
            records.removeIf(e -> !seen.add(e.getId()));
            Collections.sort(records);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return records;
    }

    @Override
    public List<Skill> fetchSkillsByIndustryPositionIds(Integer industryId, List<Integer> positionIds) {
        List<Skill> records = null;
        try {
            records = dsl.select(SKILL.fields()).from(SKILL)
                    .join(INDUSTRY_AND_POSITIONS_SKILL)
                    .on(INDUSTRY_AND_POSITIONS_SKILL.SKILL_ID.eq(SKILL.ID))
                    .where(INDUSTRY_AND_POSITIONS_SKILL.POSITION_ID.in(CollectionUtils.emptyIfNull(positionIds)))
                    .and(INDUSTRY_AND_POSITIONS_SKILL.INDUSTRY_ID.eq(industryId))
                    .orderBy(SKILL.ID.asc())
                    .fetchInto(Skill.class);
            List<Skill> agnosticSkills = dsl.select(SKILL.fields()).from(SKILL)
                    .join(INDUSTRY_AGNOSTIC_SKILL)
                    .on(INDUSTRY_AGNOSTIC_SKILL.SKILL_ID.eq(SKILL.ID))
                    .where(INDUSTRY_AGNOSTIC_SKILL.POSITION_ID.in(CollectionUtils.emptyIfNull(positionIds)))
                    .orderBy(SKILL.ID.asc())
                    .fetchInto(Skill.class);
            records.addAll(agnosticSkills);
            HashSet<Object> seen = new HashSet<>();
            records.removeIf(e -> !seen.add(e.getId()));
            Collections.sort(records);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return records;
    }

    /**
     * Ontology Editor
     */


    @Override
    public List<PositionWithAgnosticMapping> fetchAllPosition(Boolean agnosticIndustry, String section) {
        Condition condition = trueCondition();
        if (agnosticIndustry != null && section != null) {
            if (section.equalsIgnoreCase("product_service")) {
                condition = POSITION_AGNOSTIC_MAPPING.AGNOSTIC_PRODUCTS_SERVICES
                        .eq(agnosticIndustry).or(POSITION_AGNOSTIC_MAPPING.AGNOSTIC_PRODUCTS_SERVICES.isNull());
            } else if (section.equalsIgnoreCase("skills")) {
                condition = POSITION_AGNOSTIC_MAPPING.AGNOSTIC_SKILL
                        .eq(agnosticIndustry).or(POSITION_AGNOSTIC_MAPPING.AGNOSTIC_SKILL.isNull());
            } else if (section.equalsIgnoreCase("qualification")) {
                condition = POSITION_AGNOSTIC_MAPPING.AGNOSTIC_QUALIFICATION
                        .eq(agnosticIndustry).or(POSITION_AGNOSTIC_MAPPING.AGNOSTIC_QUALIFICATION.isNull());
            }
        }

        return dsl.select(POSITION.ID,
                POSITION.DESCRIPTION,
                POSITION_AGNOSTIC_MAPPING.AGNOSTIC_PRODUCTS_SERVICES.as("agnostic_products_and_services"),
                POSITION_AGNOSTIC_MAPPING.AGNOSTIC_QUALIFICATION,
                POSITION_AGNOSTIC_MAPPING.AGNOSTIC_SKILL)
                .from(POSITION)
                .leftJoin(POSITION_AGNOSTIC_MAPPING).on(POSITION_AGNOSTIC_MAPPING.POSITION_ID.eq(POSITION.ID))
                .where(condition)
                .orderBy(POSITION.ID)
                .fetchInto(PositionWithAgnosticMapping.class);
    }

    @Override
    public List<PositionWithAgnosticMapping> fetchPositionsByIds(List<Integer> positionIds) {
        return dsl.select(POSITION.ID,
                POSITION.DESCRIPTION,
                POSITION_AGNOSTIC_MAPPING.AGNOSTIC_PRODUCTS_SERVICES.as("agnostic_products_and_services"),
                POSITION_AGNOSTIC_MAPPING.AGNOSTIC_QUALIFICATION,
                POSITION_AGNOSTIC_MAPPING.AGNOSTIC_SKILL)
                .from(POSITION)
                .leftJoin(POSITION_AGNOSTIC_MAPPING).on(POSITION_AGNOSTIC_MAPPING.POSITION_ID.eq(POSITION.ID))
                .where(POSITION.ID.in(positionIds))
                .orderBy(POSITION.ID)
                .fetchInto(PositionWithAgnosticMapping.class);
    }

    @Override
    public List<ProductsAndServices> fetchAllProductsAndServices() {
        return dsl.select().from(PRODUCTS_SERVICES).orderBy(PRODUCTS_SERVICES.ID).fetchInto(ProductsAndServices.class);
    }

    @Override
    public List<Qualification> fetchAllQualifications() {
        return dsl.select().from(QUALIFICATION).orderBy(QUALIFICATION.ID).fetchInto(Qualification.class);
    }

    @Override
    public List<Skill> fetchAllSkills() {
        return dsl.select().from(SKILL).orderBy(SKILL.ID).fetchInto(Skill.class);
    }

    @Override
    public void createSkill(za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Skill skill) {
        try {
            SkillRecord record = dsl.newRecord(SKILL, skill);
            record.store();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateSkill(Integer id, String name) {
        dsl.update(SKILL)
                .set(SKILL.NAME, name)
                .where(SKILL.ID.eq(id))
                .execute();
    }

    @Override
    public void createProductsAndServices(ProductsServices productsServices) {
        ProductsServicesRecord record = dsl.newRecord(PRODUCTS_SERVICES, productsServices);
        record.store();
    }

    @Override
    public void updateProductsAndServices(Integer id, String name) {
        dsl.update(PRODUCTS_SERVICES)
                .set(PRODUCTS_SERVICES.NAME, name)
                .where(PRODUCTS_SERVICES.ID.eq(id))
                .execute();
    }

    @Override
    public void createQualifications(za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Qualification qualification) {
        QualificationRecord record = dsl.newRecord(QUALIFICATION, qualification);
        record.store();
    }

    @Override
    public void updateQualifications(Integer id, String name) {
        dsl.update(QUALIFICATION)
                .set(QUALIFICATION.NAME, name)
                .where(QUALIFICATION.ID.eq(id))
                .execute();
    }

    @Override
    public Position createPosition(Position position) {
        PositionRecord record = dsl.newRecord(POSITION, position);
        record.store();
        position.setId(Integer.valueOf(record.getId()));
        return position;
    }

    @Override
    public void updatePosition(Integer id, String name) {
        dsl.update(POSITION)
                .set(POSITION.DESCRIPTION, name)
                .where(POSITION.ID.eq(id))
                .execute();
    }

    @Override
    public void createPositionAgnosticMapping(PositionAgnosticMapping positionAgnosticMapping) {
        PositionAgnosticMappingRecord record = dsl.newRecord(POSITION_AGNOSTIC_MAPPING, positionAgnosticMapping);
        record.store();
    }

    @Override
    public List<PositionFunctionMapping> fetchAllPositionFunctionMapping() {
        try (ResultSet rs = dsl.select(
                POSITION.ID.as("position_id"),
                POSITION.DESCRIPTION.as("position_description"),
                FUNCTION.ID.as("function_id"),
                FUNCTION.DESCRIPTION.as("function_description"))
                .from(POSITIONS_FUNCTION)
                .join(POSITION).on(POSITION.ID.eq(POSITIONS_FUNCTION.POSITION_ID))
                .join(FUNCTION).on(FUNCTION.ID.eq(POSITIONS_FUNCTION.FUNCTION_ID))
                .fetchResultSet()) {

            JdbcMapper<PositionFunction> mapper = JdbcMapperFactory
                    .newInstance()
                    .newMapper(new TypeReference<PositionFunction>() {
                    });

            Stream<PositionFunction> stream = mapper.stream(rs);
            final List<PositionFunction> results = stream.collect(toList());

            Map<CommonOntologyObject, List<CommonOntologyObject>> intermediateMap = results.stream()
                    .collect(
                            Collectors.groupingBy(
                                    PositionFunction::getFunction,
                                    mapping(PositionFunction::getPosition, toList())
                            )
                    );

            return intermediateMap.entrySet().parallelStream().map(entry -> {
                return new PositionFunctionMapping(entry.getKey().getId(), entry.getKey().getDescription(), entry.getValue());
            }).sorted(Comparator.comparingInt(PositionFunctionMapping::getId))
                    .collect(toList());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Optional<PositionsFunction> checkIfMappingAlreadyExist(Integer positionId, Integer functionId) {
        return dsl.select().from(POSITIONS_FUNCTION)
                .where(POSITIONS_FUNCTION.FUNCTION_ID.eq(functionId))
                .and(POSITIONS_FUNCTION.POSITION_ID.eq(positionId))
                .fetchOptionalInto(PositionsFunction.class);
    }

    @Override
    public void createPositionsFunction(Integer positionId, Integer functionId) {
        PositionsFunction positionsFunction = new PositionsFunction();
        positionsFunction.setFunctionId(functionId);
        positionsFunction.setPositionId(positionId);
        PositionsFunctionRecord record = dsl.newRecord(POSITIONS_FUNCTION, positionsFunction);
        record.store();
    }

    @Override
    public void deletePositionsFunction(Integer positionId, Integer functionId) {
        dsl.delete(POSITIONS_FUNCTION)
                .where(POSITIONS_FUNCTION.FUNCTION_ID.eq(functionId))
                .and(POSITIONS_FUNCTION.POSITION_ID.eq(positionId))
                .execute();
    }

    @Override
    public List<OntologyMaster> fetchAllOntologyMapping() {
        return dsl.select().from(ONTOLOGY_MASTER).fetchInto(OntologyMaster.class);
    }

    @Override
    public Optional<IndustryAndPositionsSkill> checkIfIndustryAndPositionsSkillExist(Integer industryId, Integer positionId, Integer skillId) {
        return dsl.select().from(INDUSTRY_AND_POSITIONS_SKILL)
                .where(INDUSTRY_AND_POSITIONS_SKILL.INDUSTRY_ID.eq(industryId))
                .and(INDUSTRY_AND_POSITIONS_SKILL.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AND_POSITIONS_SKILL.SKILL_ID.eq(skillId))
                .fetchOptionalInto(IndustryAndPositionsSkill.class);
    }

    @Override
    public Optional<IndustryAndPostionsProductsServices> checkIfIndustryAndPositionsProductsServicesExist(Integer industryId, Integer positionId, Integer productsServicesId) {
        return dsl.select().from(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES)
                .where(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES.INDUSTRY_ID.eq(industryId))
                .and(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES.PRODUCT_SERVICE_ID.eq(productsServicesId))
                .fetchOptionalInto(IndustryAndPostionsProductsServices.class);
    }

    @Override
    public Optional<IndustryAndPostionsQualification> checkIfIndustryAndPositionsQualificationExist(Integer industryId, Integer positionId, Integer qualificationId) {
        return dsl.select().from(INDUSTRY_AND_POSTIONS_QUALIFICATION)
                .where(INDUSTRY_AND_POSTIONS_QUALIFICATION.INDUSTRY_ID.eq(industryId))
                .and(INDUSTRY_AND_POSTIONS_QUALIFICATION.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AND_POSTIONS_QUALIFICATION.QUALIFICATION_ID.eq(qualificationId))
                .fetchOptionalInto(IndustryAndPostionsQualification.class);
    }

    @Override
    public Optional<IndustryAgnosticSkill> checkIfIndustryAgnosticSkillExist(Integer positionId, Integer skillId) {
        return dsl.select().from(INDUSTRY_AGNOSTIC_SKILL)
                .where(INDUSTRY_AGNOSTIC_SKILL.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AGNOSTIC_SKILL.SKILL_ID.eq(skillId))
                .fetchOptionalInto(IndustryAgnosticSkill.class);
    }

    @Override
    public Optional<IndustryAgnosticProductsServices> checkIfIndustryAgnosticProductsServicesExist(Integer positionId, Integer productsServicesId) {
        return dsl.select().from(INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES)
                .where(INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES.PRODUCT_SERVICE_ID.eq(productsServicesId))
                .fetchOptionalInto(IndustryAgnosticProductsServices.class);
    }

    @Override
    public Optional<IndustryAgnosticQualification> checkIfIndustryAgnosticQualificationExist(Integer positionId, Integer qualificationId) {
        return dsl.select().from(INDUSTRY_AGNOSTIC_QUALIFICATION)
                .where(INDUSTRY_AGNOSTIC_QUALIFICATION.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AGNOSTIC_QUALIFICATION.QUALIFICATION_ID.eq(qualificationId))
                .fetchOptionalInto(IndustryAgnosticQualification.class);
    }

    @Override
    public void createIndustryAndPositionsSkill(Integer industryId, Integer positionId, Integer skillId) {
        IndustryAndPositionsSkill industryAndPositionsSkill = new IndustryAndPositionsSkill();
        industryAndPositionsSkill.setIndustryId(industryId);
        industryAndPositionsSkill.setPositionId(positionId);
        industryAndPositionsSkill.setSkillId(skillId);
        IndustryAndPositionsSkillRecord record = dsl.newRecord(INDUSTRY_AND_POSITIONS_SKILL, industryAndPositionsSkill);
        record.store();
    }

    @Override
    public void createIndustryAndPostionsProductsServices(Integer industryId, Integer positionId, Integer productsServicesId) {
        IndustryAndPostionsProductsServices industryAndPostionsProductsServices = new IndustryAndPostionsProductsServices();
        industryAndPostionsProductsServices.setIndustryId(industryId);
        industryAndPostionsProductsServices.setPositionId(positionId);
        industryAndPostionsProductsServices.setProductServiceId(productsServicesId);
        IndustryAndPostionsProductsServicesRecord record = dsl.newRecord(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES, industryAndPostionsProductsServices);
        record.store();
    }

    @Override
    public void createIndustryAndPostionsQualification(Integer industryId, Integer positionId, Integer qualificationId) {
        IndustryAndPostionsQualification industryAndPostionsQualification = new IndustryAndPostionsQualification();
        industryAndPostionsQualification.setIndustryId(industryId);
        industryAndPostionsQualification.setPositionId(positionId);
        industryAndPostionsQualification.setQualificationId(qualificationId);
        IndustryAndPostionsQualificationRecord record = dsl.newRecord(INDUSTRY_AND_POSTIONS_QUALIFICATION, industryAndPostionsQualification);
        record.store();
    }

    @Override
    public void createIndustryAgnosticSkill(Integer positionId, Integer skillId) {
        IndustryAgnosticSkill industryAgnosticSkill = new IndustryAgnosticSkill();
        industryAgnosticSkill.setPositionId(positionId);
        industryAgnosticSkill.setSkillId(skillId);
        IndustryAgnosticSkillRecord record = dsl.newRecord(INDUSTRY_AGNOSTIC_SKILL, industryAgnosticSkill);
        record.store();
    }

    @Override
    public void createIndustryAgnosticProductsServices(Integer positionId, Integer productsServicesId) {
        IndustryAgnosticProductsServices industryAgnosticProductsService = new IndustryAgnosticProductsServices();
        industryAgnosticProductsService.setPositionId(positionId);
        industryAgnosticProductsService.setProductServiceId(productsServicesId);
        IndustryAgnosticProductsServicesRecord record = dsl.newRecord(INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES, industryAgnosticProductsService);
        record.store();
    }

    @Override
    public void createIndustryAgnosticQualification(Integer positionId, Integer qualificationId) {
        IndustryAgnosticQualification industryAgnosticQualification = new IndustryAgnosticQualification();
        industryAgnosticQualification.setPositionId(positionId);
        industryAgnosticQualification.setQualificationId(qualificationId);
        IndustryAgnosticQualificationRecord record = dsl.newRecord(INDUSTRY_AGNOSTIC_QUALIFICATION, industryAgnosticQualification);
        record.store();
    }

    @Override
    public List<IndustryPositionProductsServicesGroup> fetchIndustryPositionProductsServicesGroup() {
        return dsl.select().from(INDUSTRY_POSITION_PRODUCTS_SERVICES_GROUP).fetchInto(IndustryPositionProductsServicesGroup.class);
    }

    @Override
    public List<IndustryPositionQualificationsGroup> fetchIndustryPositionQualificationsGroup() {
        return dsl.select().from(INDUSTRY_POSITION_QUALIFICATIONS_GROUP).fetchInto(IndustryPositionQualificationsGroup.class);
    }

    @Override
    public List<IndustryPositionSkillsGroup> fetchIndustryPositionSkillsGroup() {
        return dsl.select().from(INDUSTRY_POSITION_SKILLS_GROUP).fetchInto(IndustryPositionSkillsGroup.class);
    }

    @Override
    public void deleteIndustryAndPostionsProductsServices(Integer industryId, Integer positionId, Integer productsServicesId) {
        dsl.delete(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES)
                .where(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES.INDUSTRY_ID.eq(industryId))
                .and(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AND_POSTIONS_PRODUCTS_SERVICES.PRODUCT_SERVICE_ID.eq(productsServicesId))
                .execute();
    }

    @Override
    public void deleteIndustryAndPostionsQualification(Integer industryId, Integer positionId, Integer qualificationId) {
        dsl.delete(INDUSTRY_AND_POSTIONS_QUALIFICATION)
                .where(INDUSTRY_AND_POSTIONS_QUALIFICATION.INDUSTRY_ID.eq(industryId))
                .and(INDUSTRY_AND_POSTIONS_QUALIFICATION.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AND_POSTIONS_QUALIFICATION.QUALIFICATION_ID.eq(qualificationId))
                .execute();
    }

    @Override
    public void deleteIndustryAndPositionsSkill(Integer industryId, Integer positionId, Integer skillId) {
        dsl.delete(INDUSTRY_AND_POSITIONS_SKILL)
                .where(INDUSTRY_AND_POSITIONS_SKILL.INDUSTRY_ID.eq(industryId))
                .and(INDUSTRY_AND_POSITIONS_SKILL.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AND_POSITIONS_SKILL.SKILL_ID.eq(skillId))
                .execute();
    }

    @Override
    public void deleteIndustryAgnosticProductsServices(Integer positionId, Integer productsServicesId) {
        dsl.delete(INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES)
                .where(INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES.PRODUCT_SERVICE_ID.eq(productsServicesId))
                .execute();
    }

    @Override
    public void deleteIndustryAgnosticQualification(Integer positionId, Integer qualificationId) {
        dsl.delete(INDUSTRY_AGNOSTIC_QUALIFICATION)
                .where(INDUSTRY_AGNOSTIC_QUALIFICATION.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AGNOSTIC_QUALIFICATION.QUALIFICATION_ID.eq(qualificationId))
                .execute();
    }

    @Override
    public void deleteIndustryAgnosticSkill(Integer positionId, Integer skillId) {
        dsl.delete(INDUSTRY_AGNOSTIC_SKILL)
                .where(INDUSTRY_AGNOSTIC_SKILL.POSITION_ID.eq(positionId))
                .and(INDUSTRY_AGNOSTIC_SKILL.SKILL_ID.eq(skillId))
                .execute();
    }

    @Override
    public PositionWithAgnosticMapping getPositionById(Integer id) {
        return dsl.select(POSITION.ID,
                POSITION.DESCRIPTION,
                POSITION_AGNOSTIC_MAPPING.AGNOSTIC_PRODUCTS_SERVICES.as("agnostic_products_and_services"),
                POSITION_AGNOSTIC_MAPPING.AGNOSTIC_QUALIFICATION,
                POSITION_AGNOSTIC_MAPPING.AGNOSTIC_SKILL)
                .from(POSITION)
                .leftJoin(POSITION_AGNOSTIC_MAPPING).on(POSITION_AGNOSTIC_MAPPING.POSITION_ID.eq(POSITION.ID))
                .where(POSITION.ID.eq(id))
                .fetchOneInto(PositionWithAgnosticMapping.class);
    }

    @Override
    public void updatePositionAgnosticMapping(Integer id, Boolean agnosticProductsAndServices, Boolean agnosticQualification, Boolean agnosticSkill) {
        Map<TableField, Object> map = new HashMap<>();
        if (agnosticProductsAndServices != null) {
            map.put(POSITION_AGNOSTIC_MAPPING.AGNOSTIC_PRODUCTS_SERVICES, agnosticProductsAndServices);
        }
        if (agnosticQualification != null) {
            map.put(POSITION_AGNOSTIC_MAPPING.AGNOSTIC_QUALIFICATION, agnosticQualification);
        }
        if (agnosticSkill != null) {
            map.put(POSITION_AGNOSTIC_MAPPING.AGNOSTIC_SKILL, agnosticSkill);
        }
        dsl.update(POSITION_AGNOSTIC_MAPPING)
                .set(map)
                .where(POSITION_AGNOSTIC_MAPPING.POSITION_ID.equal(id))
                .execute();
    }
}