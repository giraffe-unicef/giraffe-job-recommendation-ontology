package za.co.giraffe.springboot_starter.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PositionRequest {
    @JsonProperty("name")
    private String name;
    @JsonProperty("agnostic_products_and_services")
    private Boolean agnosticProductsAndServices;
    @JsonProperty("agnostic_qualification")
    private Boolean agnosticQualification;
    @JsonProperty("agnostic_skill")
    private Boolean agnosticSkill;
}
