package za.co.giraffe.springboot_starter.model.ontology;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PositionWithAgnosticMapping {
    private Integer id;
    private String description;
    @JsonProperty("agnostic_products_and_services")
    private Boolean agnosticProductsAndServices;
    @JsonProperty("agnostic_qualification")
    private Boolean agnosticQualification;
    @JsonProperty("agnostic_skill")
    private Boolean agnosticSkill;
}
