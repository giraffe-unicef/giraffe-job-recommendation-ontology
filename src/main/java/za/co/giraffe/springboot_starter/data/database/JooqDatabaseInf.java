package za.co.giraffe.springboot_starter.data.database;

import org.jooq.DSLContext;
import za.co.giraffe.infrastructure.database.DatabaseInf;

public interface JooqDatabaseInf extends DatabaseInf {
    void withTransaction(DSLContext context);
}
