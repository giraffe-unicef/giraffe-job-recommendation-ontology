package za.co.giraffe.springboot_starter.model.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class OntologyEditorRequest {
    @NotEmpty
    private String name;
}
