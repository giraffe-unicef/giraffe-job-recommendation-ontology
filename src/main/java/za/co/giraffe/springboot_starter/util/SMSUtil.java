package za.co.giraffe.springboot_starter.util;

import org.apache.commons.lang3.text.WordUtils;

import java.util.Map;

public class SMSUtil {
    public static String utf8ToLatin1(String s) {
        String cleanedString = s;
        cleanedString = s.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        cleanedString = cleanedString.replaceAll("[\u00AB\u2034\u2037\u00BB\u02BA\u030B\u030E\u201C\u201D\u201E\u201F\u2033\u2036\u3003\u301D\u301E]", "\"");
        cleanedString = cleanedString.replaceAll("[\u02CB\u0300\u2035]", "`");
        cleanedString = cleanedString.replaceAll("[\u02C4\u02C6\u0302\u2038\u2303]", "^");
        cleanedString = cleanedString.replaceAll("[\u02CD\u0331\u0332\u2017]", "_");
        cleanedString = cleanedString.replaceAll("[\u00AD\u2010\u2011\u2012\u2013\u2014\u2212\u2015]", "-");
        cleanedString = cleanedString.replaceAll("[\u201A]", ",");
        cleanedString = cleanedString.replaceAll("[\u0589\u05C3\u2236]", ":");
        cleanedString = cleanedString.replaceAll("[\u01C3\u2762]", "!");
        cleanedString = cleanedString.replaceAll("[\u203D]", "?");
        cleanedString = cleanedString.replaceAll("[\u00B4\u02B9\u02BC\u02C8\u0301\u200B\u2018\u2019\u201B\u2032]", "'");
        cleanedString = cleanedString.replaceAll("[\u27E6]", "[");
        cleanedString = cleanedString.replaceAll("[\u301B]", "]");
        cleanedString = cleanedString.replaceAll("[\u2983]", "{");
        cleanedString = cleanedString.replaceAll("[\u2984]", "}");
        cleanedString = cleanedString.replaceAll("[\u066D\u204E\u2217\u2731]", "*");
        cleanedString = cleanedString.replaceAll("[\u00F7\u0338\u2044\u2060\u2215]", "/");
        cleanedString = cleanedString.replaceAll("[\u20E5\u2216]", "\\");
        cleanedString = cleanedString.replaceAll("[\u266F]", "#");
        cleanedString = cleanedString.replaceAll("[\u066A\u2052]", "%");
        cleanedString = cleanedString.replaceAll("[\u2039\u2329\u27E8\u3008]", "<");
        cleanedString = cleanedString.replaceAll("[\u203A\u232A\u27E9\u3009]", ">");
        cleanedString = cleanedString.replaceAll("[\u01C0\u05C0\u2223\u2758]", "|");
        cleanedString = cleanedString.replaceAll("[\u02DC\u0303\u2053\u223C\u301C]", "~");
        return cleanedString;
    }

    public static String cleanName(String name) {
        return WordUtils.capitalize(name).trim();
    }

    public static String shortenedName(String name) {
        return cleanName(name.trim().split(" ")[0]);
    }

    public static String getFirstFirstName(String name) {
        final String[] split = name.split(" ");
        return WordUtils.capitalize(split[0]);
    }

    public static String replace(String s, Map<String, String> m) {
        for (Map.Entry<String, String> entry : m.entrySet()) {
            s = s.replace(entry.getKey().toString(), entry.getValue().toString());
        }
        return s;
    }

    public static int numberOfSMSs(String message) {
        int messageLength = message.length();
        if (messageLength <= 160) {
            return 1;
        } else {
            return (int) Math.ceil(messageLength / 160.0);
        }
    }
}
