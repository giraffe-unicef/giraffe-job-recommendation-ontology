package za.co.giraffe.springboot_starter.model.api_util;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

/**
 * StarterObject
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-02-26T08:11:33.670Z[GMT]")
public class StarterObject {
    @JsonProperty("id")
    private UUID id = null;

    @JsonProperty("example_link")
    private Link exampleLink = null;

    public StarterObject id(UUID id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public StarterObject exampleLink(Link exampleLink) {
        this.exampleLink = exampleLink;
        return this;
    }

    /**
     * Get exampleLink
     *
     * @return exampleLink
     **/
    @ApiModelProperty(value = "")

    @Valid
    public Link getExampleLink() {
        return exampleLink;
    }

    public void setExampleLink(Link exampleLink) {
        this.exampleLink = exampleLink;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StarterObject starterObject = (StarterObject) o;
        return Objects.equals(this.id, starterObject.id) &&
                Objects.equals(this.exampleLink, starterObject.exampleLink);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, exampleLink);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class StarterObject {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    exampleLink: ").append(toIndentedString(exampleLink)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
