package za.co.giraffe.springboot_starter.api.ontology;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryPositionProductsServicesGroup;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryPositionQualificationsGroup;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryPositionSkillsGroup;
import za.co.giraffe.springboot_starter.model.ontology.*;
import za.co.giraffe.springboot_starter.model.request.*;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Ontology Editor API", tags = {"Ontology Editor API"})
@RequestMapping("/ontology")
public interface OntologyEditorApi {

    @ApiOperation(value = "Get all positions", nickname = "getALlPositions", notes = "",
            response = PositionWithAgnosticMapping.class, responseContainer = "List", tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = PositionWithAgnosticMapping.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/positions",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<PositionWithAgnosticMapping>> getALlPositions(
            @RequestParam(name = "agnostic_industry", required = false) Boolean agnosticIndustry,
            @RequestParam(name = "section", required = false) String section
    );

    @ApiOperation(value = "Get all products and services", nickname = "getAllProductsAndServices",
            notes = "", response = ProductsAndServices.class, responseContainer = "List", tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = ProductsAndServices.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/products-and-services",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<ProductsAndServices>> getAllProductsAndServices();

    @ApiOperation(value = "Get all qualifications", nickname = "getAllQualifications",
            notes = "", response = Qualification.class, responseContainer = "List", tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Qualification.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/qualifications",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Qualification>> getAllQualifications();

    @ApiOperation(value = "Get all skills", nickname = "getAllSkills",
            notes = "", response = Skill.class, responseContainer = "List", tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Skill.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/skills",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Skill>> getAllSkills();


    @ApiOperation(value = "Create positions", nickname = "createPositions", notes = "",
            response = PositionWithAgnosticMapping.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = PositionWithAgnosticMapping.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/positions",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<PositionWithAgnosticMapping> createPositions(@ApiParam(value = "") @Valid @RequestBody PositionRequest body);

    @ApiOperation(value = "Create products and services", nickname = "createProductsAndServices",
            notes = "", response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/products-and-services",
            method = RequestMethod.POST)
    ResponseEntity<String> createProductsAndServices(@ApiParam(value = "") @Valid @RequestBody OntologyEditorRequest ontologyEditorRequest);

    @ApiOperation(value = "Create qualifications", nickname = "createQualifications",
            notes = "", response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/qualifications",
            method = RequestMethod.POST)
    ResponseEntity<String> createQualifications(@ApiParam(value = "") @Valid @RequestBody OntologyEditorRequest ontologyEditorRequest);

    @ApiOperation(value = "Create skills", nickname = "createSkills",
            notes = "", response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/skills",
            method = RequestMethod.POST)
    ResponseEntity<String> createSkills(@ApiParam(value = "") @Valid @RequestBody OntologyEditorRequest ontologyEditorRequest);

    @ApiOperation(value = "Update positions", nickname = "updatePositions", notes = "",
            response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/positions/{id}",
            method = RequestMethod.PUT)
    ResponseEntity<String> updatePositions(@ApiParam(value = "Position ID") @PathVariable("id") Integer positionId,
                                           @ApiParam(value = "") @Valid @RequestBody OntologyEditorRequest ontologyEditorRequest);

    @ApiOperation(value = "Update products and services", nickname = "updateProductsAndServices",
            notes = "", response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/products-and-services/{id}",
            method = RequestMethod.PUT)
    ResponseEntity<String> updateProductsAndServices(@ApiParam(value = "Products and services ID") @PathVariable("id") Integer productAndServicesId,
                                                     @ApiParam(value = "") @Valid @RequestBody OntologyEditorRequest ontologyEditorRequest);

    @ApiOperation(value = "Update qualifications", nickname = "updateQualifications",
            notes = "", response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/qualifications/{id}",
            method = RequestMethod.PUT)
    ResponseEntity<String> updateQualifications(@ApiParam(value = "Qualifications ID") @PathVariable("id") Integer qualificationsId,
                                                @ApiParam(value = "") @Valid @RequestBody OntologyEditorRequest ontologyEditorRequest);

    @ApiOperation(value = "Update skills", nickname = "updateSkills",
            notes = "", response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/skills/{id}",
            method = RequestMethod.PUT)
    ResponseEntity<String> updateSkills(@ApiParam(value = "Skills ID") @PathVariable("id") Integer skillsId,
                                        @ApiParam(value = "") @Valid @RequestBody OntologyEditorRequest ontologyEditorRequest);

    @ApiOperation(value = "Get all positions with categories", nickname = "getALlPositionsFunctionMapping", notes = "",
            response = PositionFunctionMapping.class, responseContainer = "List", tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = PositionFunctionMapping.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/positions-categories",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<PositionFunctionMapping>> getALlPositionsFunctionMapping();


    @ApiOperation(value = "Create positions categories mapping", nickname = "createPositionsFunctionMapping", notes = "",
            response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/positions-categories",
            method = RequestMethod.POST)
    ResponseEntity<String> createPositionsFunctionMapping(@ApiParam(value = "") @Valid @RequestBody UpsertPositionFunctionMapping upsertPositionFunctionMapping);


    @ApiOperation(value = "Get all Industry Position Products and Services Group Mapping", nickname = "getIndustryPositionProductsServicesGroup", notes = "",
            response = IndustryPositionProductsServicesGroup.class, responseContainer = "List", tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = IndustryPositionProductsServicesGroup.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/products-services-group-mapping",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<IndustryPositionProductsServicesGroup>> getIndustryPositionProductsServicesGroup();

    @ApiOperation(value = "Get all Industry Position Qualifications Group Mapping", nickname = "getIndustryPositionQualificationsGroup", notes = "",
            response = IndustryPositionQualificationsGroup.class, responseContainer = "List", tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = IndustryPositionQualificationsGroup.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/qualifications-group-mapping",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<IndustryPositionQualificationsGroup>> getIndustryPositionQualificationsGroup();

    @ApiOperation(value = "Get all Industry Position Skills Group Mapping", nickname = "getIndustryPositionSkillsGroup", notes = "",
            response = IndustryPositionSkillsGroup.class, responseContainer = "List", tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = IndustryPositionSkillsGroup.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/skills-group-mapping",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<IndustryPositionSkillsGroup>> getIndustryPositionSkillsGroup();


    @ApiOperation(value = "Update Industry Position Products and Services Group Mapping", nickname = "updateIndustryPositionProductAndServiceGroupMapping", notes = "",
            response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/products-services-group-mapping",
            method = RequestMethod.PUT)
    ResponseEntity<String> updateIndustryPositionProductAndServiceGroupMapping(@ApiParam(value = "") @Valid @RequestBody UpdateOntologyMapping updateOntologyMapping);

    @ApiOperation(value = "Update Industry Position Qualification Group Mapping", nickname = "updateIndustryPositionQualificationsGroupMapping", notes = "",
            response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/qualifications-group-mapping",
            method = RequestMethod.PUT)
    ResponseEntity<String> updateIndustryPositionQualificationsGroupMapping(@ApiParam(value = "") @Valid @RequestBody UpdateOntologyMapping updateOntologyMapping);

    @ApiOperation(value = "Update Industry Position Skills Group Mapping", nickname = "updateIndustryPositionSkillsGroupMapping", notes = "",
            response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/skills-group-mapping",
            method = RequestMethod.PUT)
    ResponseEntity<String> updateIndustryPositionSkillsGroupMapping(@ApiParam(value = "") @Valid @RequestBody UpdateOntologyMapping updateOntologyMapping);

    @ApiOperation(value = "Create Industry Position Products and Services Group Mapping", nickname = "createIndustryPositionProductAndServiceGroupMapping", notes = "",
            response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/products-services-group-mapping",
            method = RequestMethod.POST)
    ResponseEntity<String> createIndustryPositionProductAndServiceGroupMapping(@ApiParam(value = "") @Valid @RequestBody CreateOntologyMapping createOntologyMapping);

    @ApiOperation(value = "Create Industry Position Qualification Group Mapping", nickname = "createIndustryPositionQualificationsGroupMapping", notes = "",
            response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/qualifications-group-mapping",
            method = RequestMethod.POST)
    ResponseEntity<String> createIndustryPositionQualificationsGroupMapping(@ApiParam(value = "") @Valid @RequestBody CreateOntologyMapping createOntologyMapping);

    @ApiOperation(value = "Create Industry Position Skills Group Mapping", nickname = "createIndustryPositionSkillsGroupMapping", notes = "",
            response = String.class, tags = {"Ontology Editor API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = String.class),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/skills-group-mapping",
            method = RequestMethod.POST)
    ResponseEntity<String> createIndustryPositionSkillsGroupMapping(@ApiParam(value = "") @Valid @RequestBody CreateOntologyMapping createOntologyMapping);


}
