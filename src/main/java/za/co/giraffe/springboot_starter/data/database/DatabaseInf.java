package za.co.giraffe.springboot_starter.data.database;

import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseInf {
    Connection getConnection() throws SQLException;
}
