/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables.daos;


import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;
import za.co.giraffe.infrastructure.entities.jooq.tables.IndustrysSector;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.IndustrysSectorRecord;

import javax.annotation.Generated;
import java.util.List;
import java.util.UUID;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.jooq.org",
                "jOOQ version:3.11.9"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class IndustrysSectorDao extends DAOImpl<IndustrysSectorRecord, za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustrysSector, Integer> {

    /**
     * Create a new IndustrysSectorDao without any configuration
     */
    public IndustrysSectorDao() {
        super(IndustrysSector.INDUSTRYS_SECTOR, za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustrysSector.class);
    }

    /**
     * Create a new IndustrysSectorDao with an attached configuration
     */
    public IndustrysSectorDao(Configuration configuration) {
        super(IndustrysSector.INDUSTRYS_SECTOR, za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustrysSector.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer getId(za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustrysSector object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustrysSector> fetchById(Integer... values) {
        return fetch(IndustrysSector.INDUSTRYS_SECTOR.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustrysSector fetchOneById(Integer value) {
        return fetchOne(IndustrysSector.INDUSTRYS_SECTOR.ID, value);
    }

    /**
     * Fetch records that have <code>industry_id IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustrysSector> fetchByIndustryId(Integer... values) {
        return fetch(IndustrysSector.INDUSTRYS_SECTOR.INDUSTRY_ID, values);
    }

    /**
     * Fetch records that have <code>sector_id IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustrysSector> fetchBySectorId(UUID... values) {
        return fetch(IndustrysSector.INDUSTRYS_SECTOR.SECTOR_ID, values);
    }
}
