package za.co.giraffe.springboot_starter.api.ontology.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Position;
import za.co.giraffe.springboot_starter.api.ontology.OntologyApi;
import za.co.giraffe.springboot_starter.model.ess.Function;
import za.co.giraffe.springboot_starter.model.ess.Industry;
import za.co.giraffe.springboot_starter.model.ontology.ProductsAndServices;
import za.co.giraffe.springboot_starter.model.ontology.Qualification;
import za.co.giraffe.springboot_starter.model.ontology.Skill;
import za.co.giraffe.springboot_starter.services.ontology.IOntologyService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-05-16T13:33:34.418Z[GMT]")
@Controller
public class OntologyApiController implements OntologyApi {

    private static final Logger log = LoggerFactory.getLogger(OntologyApiController.class);
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;
    private IOntologyService iOntologyService;

    @org.springframework.beans.factory.annotation.Autowired
    public OntologyApiController(ObjectMapper objectMapper, HttpServletRequest request, IOntologyService iOntologyService) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.iOntologyService = iOntologyService;
    }

    @Override
    public ResponseEntity<List<Function>> getAllFunctions() {
        try {
            List<Function> functions = iOntologyService.handleFetchFunctions();
            if (functions == null || functions.isEmpty()) {
                return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<Function>>(functions, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<Position>> getPositionsByFunctionId
            (@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "function_id", required = true) Integer functionId) {
        try {
            List<Position> positions = iOntologyService.handleFetchPositionsByFunctionId(functionId);
            if (positions == null || positions.isEmpty()) {
                return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<Position>>(positions, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<ProductsAndServices>> getProductsAndServicesByIndustryAndPositionIds
            (@ApiParam(value = "Industry ID") @Valid @RequestParam(value = "industry_id", required = true) Integer industryId,
             @ApiParam(value = "One or more position IDs") @Valid @RequestParam(value = "position_ids", required = false) List<Integer> positionIds) {
        if (industryId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try {
            List<ProductsAndServices> productsAndServices = iOntologyService.handleFetchProductsAndServicesByIndustryPositionIds(industryId, positionIds);
            if (productsAndServices == null || productsAndServices.isEmpty()) {
                return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<ProductsAndServices>>(productsAndServices, HttpStatus.OK);
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<Qualification>> getQualificationsByIndustryAndPositionIds
            (@ApiParam(value = "Industry ID") @Valid @RequestParam(value = "industry_id", required = true) Integer industryId,
             @ApiParam(value = "One or more position IDs") @Valid @RequestParam(value = "position_ids", required = false) List<Integer> positionIds) {
        if (industryId == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try {
            List<Qualification> qualifications = iOntologyService.handleFetchQualificationsByIndustryPositionIds(industryId, positionIds);
            if (qualifications == null || qualifications.isEmpty()) {
                return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<Qualification>>(qualifications, HttpStatus.OK);
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<Skill>> getSkillsByIndustryAndPositionIds
            (@ApiParam(value = "Industry ID") @Valid @RequestParam(value = "industry_id", required = true) Integer industryId,
             @ApiParam(value = "One or more position IDs") @Valid @RequestParam(value = "position_ids", required = false) List<Integer> positionIds) {
        try {
            List<Skill> skills = iOntologyService.handleFetchSkillsByIndustryPositionIds(industryId, positionIds);
            if (skills == null || skills.isEmpty()) {
                return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<Skill>>(skills, HttpStatus.OK);
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<Industry>> getAllIndustries() {
        try {
            List<Industry> industries = iOntologyService.handleFetchIndustries();
            if (industries == null || industries.isEmpty()) {
                return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<Industry>>(industries, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
