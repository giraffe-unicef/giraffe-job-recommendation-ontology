/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables.pojos;


import javax.annotation.Generated;
import java.io.Serializable;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.jooq.org",
                "jOOQ version:3.11.9"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class PositionAgnosticMapping implements Serializable {

    private static final long serialVersionUID = 758339840;

    private Integer id;
    private Integer positionId;
    private Boolean agnosticProductsServices;
    private Boolean agnosticQualification;
    private Boolean agnosticSkill;

    public PositionAgnosticMapping() {
    }

    public PositionAgnosticMapping(PositionAgnosticMapping value) {
        this.id = value.id;
        this.positionId = value.positionId;
        this.agnosticProductsServices = value.agnosticProductsServices;
        this.agnosticQualification = value.agnosticQualification;
        this.agnosticSkill = value.agnosticSkill;
    }

    public PositionAgnosticMapping(
            Integer id,
            Integer positionId,
            Boolean agnosticProductsServices,
            Boolean agnosticQualification,
            Boolean agnosticSkill
    ) {
        this.id = id;
        this.positionId = positionId;
        this.agnosticProductsServices = agnosticProductsServices;
        this.agnosticQualification = agnosticQualification;
        this.agnosticSkill = agnosticSkill;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPositionId() {
        return this.positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }

    public Boolean getAgnosticProductsServices() {
        return this.agnosticProductsServices;
    }

    public void setAgnosticProductsServices(Boolean agnosticProductsServices) {
        this.agnosticProductsServices = agnosticProductsServices;
    }

    public Boolean getAgnosticQualification() {
        return this.agnosticQualification;
    }

    public void setAgnosticQualification(Boolean agnosticQualification) {
        this.agnosticQualification = agnosticQualification;
    }

    public Boolean getAgnosticSkill() {
        return this.agnosticSkill;
    }

    public void setAgnosticSkill(Boolean agnosticSkill) {
        this.agnosticSkill = agnosticSkill;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("PositionAgnosticMapping (");

        sb.append(id);
        sb.append(", ").append(positionId);
        sb.append(", ").append(agnosticProductsServices);
        sb.append(", ").append(agnosticQualification);
        sb.append(", ").append(agnosticSkill);

        sb.append(")");
        return sb.toString();
    }
}
