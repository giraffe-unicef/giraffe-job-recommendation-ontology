package za.co.giraffe.springboot_starter.model.auth;

import lombok.Data;

@Data
public class AuthRequest {
    private String username;
    private String password;
}
