package za.co.giraffe.springboot_starter.model.ontology;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Skill implements Comparable<Skill> {
    @JsonProperty("id")
    private Integer id = null;
    @JsonProperty("name")
    private String name = null;

    public int compareTo(Skill that) {
        return this.id.compareTo(that.id);
    }
}
