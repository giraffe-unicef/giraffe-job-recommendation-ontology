package za.co.giraffe.springboot_starter.model.ontology;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CommonOntologyObject {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("description")
    private String description;
}
