package za.co.giraffe.springboot_starter.model.ontology;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Qualification implements Comparable<Qualification> {
    @JsonProperty("id")
    private Integer id = null;

    @JsonProperty("name")
    private String name = null;

    public int compareTo(Qualification that) {
        return this.id.compareTo(that.id);
    }
}
