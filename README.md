# Giraffe UNICEF Job Recommendation Ontology

Simple starter project for Spring boot that by default
* Configures JOOQ to use database credentials provides from vault
* Provides some older version JOOQ entities (just to get started)
* Basic Dockerfile
* Template for .gitlab-ci.yml

Things you should probably do 
1. Ensure that this secret has the following entries
    * auth.username
    * auth.password
    * spring.datasource.password
    * spring.datasource.username
    * spring.datasource.url
2. Regenerate the JOOQ enteries that are relevant using JOOQ-generator and update the entities in za.co.giraffe.infrastructure.entities.jooq




