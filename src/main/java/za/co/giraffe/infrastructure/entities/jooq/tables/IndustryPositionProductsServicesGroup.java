/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables;


import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import za.co.giraffe.infrastructure.entities.jooq.EssOntology;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.IndustryPositionProductsServicesGroupRecord;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.jooq.org",
                "jOOQ version:3.11.9"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class IndustryPositionProductsServicesGroup extends TableImpl<IndustryPositionProductsServicesGroupRecord> {

    /**
     * The reference instance of <code>ess_ontology.industry_position_products_services_group</code>
     */
    public static final IndustryPositionProductsServicesGroup INDUSTRY_POSITION_PRODUCTS_SERVICES_GROUP = new IndustryPositionProductsServicesGroup();
    private static final long serialVersionUID = 1579381251;
    /**
     * The column <code>ess_ontology.industry_position_products_services_group.position_ids</code>.
     */
    public final TableField<IndustryPositionProductsServicesGroupRecord, Integer[]> POSITION_IDS = createField("position_ids", org.jooq.impl.SQLDataType.INTEGER.getArrayDataType(), this, "");
    /**
     * The column <code>ess_ontology.industry_position_products_services_group.industry_ids</code>.
     */
    public final TableField<IndustryPositionProductsServicesGroupRecord, Integer[]> INDUSTRY_IDS = createField("industry_ids", org.jooq.impl.SQLDataType.INTEGER.getArrayDataType(), this, "");
    /**
     * The column <code>ess_ontology.industry_position_products_services_group.product_service_ids</code>.
     */
    public final TableField<IndustryPositionProductsServicesGroupRecord, Integer[]> PRODUCT_SERVICE_IDS = createField("product_service_ids", org.jooq.impl.SQLDataType.INTEGER.getArrayDataType(), this, "");
    /**
     * The column <code>ess_ontology.industry_position_products_services_group.agnostic_products_services</code>.
     */
    public final TableField<IndustryPositionProductsServicesGroupRecord, Boolean> AGNOSTIC_PRODUCTS_SERVICES = createField("agnostic_products_services", org.jooq.impl.SQLDataType.BOOLEAN, this, "");

    /**
     * Create a <code>ess_ontology.industry_position_products_services_group</code> table reference
     */
    public IndustryPositionProductsServicesGroup() {
        this(DSL.name("industry_position_products_services_group"), null);
    }

    /**
     * Create an aliased <code>ess_ontology.industry_position_products_services_group</code> table reference
     */
    public IndustryPositionProductsServicesGroup(String alias) {
        this(DSL.name(alias), INDUSTRY_POSITION_PRODUCTS_SERVICES_GROUP);
    }

    /**
     * Create an aliased <code>ess_ontology.industry_position_products_services_group</code> table reference
     */
    public IndustryPositionProductsServicesGroup(Name alias) {
        this(alias, INDUSTRY_POSITION_PRODUCTS_SERVICES_GROUP);
    }

    private IndustryPositionProductsServicesGroup(Name alias, Table<IndustryPositionProductsServicesGroupRecord> aliased) {
        this(alias, aliased, null);
    }

    private IndustryPositionProductsServicesGroup(Name alias, Table<IndustryPositionProductsServicesGroupRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> IndustryPositionProductsServicesGroup(Table<O> child, ForeignKey<O, IndustryPositionProductsServicesGroupRecord> key) {
        super(child, key, INDUSTRY_POSITION_PRODUCTS_SERVICES_GROUP);
    }

    /**
     * The class holding records for this type
     */
    @Override
    public Class<IndustryPositionProductsServicesGroupRecord> getRecordType() {
        return IndustryPositionProductsServicesGroupRecord.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return EssOntology.ESS_ONTOLOGY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryPositionProductsServicesGroup as(String alias) {
        return new IndustryPositionProductsServicesGroup(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryPositionProductsServicesGroup as(Name alias) {
        return new IndustryPositionProductsServicesGroup(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public IndustryPositionProductsServicesGroup rename(String name) {
        return new IndustryPositionProductsServicesGroup(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public IndustryPositionProductsServicesGroup rename(Name name) {
        return new IndustryPositionProductsServicesGroup(name, null);
    }
}
