/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables;


import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import za.co.giraffe.infrastructure.entities.jooq.EssOntology;
import za.co.giraffe.infrastructure.entities.jooq.Indexes;
import za.co.giraffe.infrastructure.entities.jooq.Keys;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.IndustryAgnosticQualificationRecord;

import javax.annotation.Generated;
import java.util.Arrays;
import java.util.List;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.jooq.org",
                "jOOQ version:3.11.9"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class IndustryAgnosticQualification extends TableImpl<IndustryAgnosticQualificationRecord> {

    /**
     * The reference instance of <code>ess_ontology.industry_agnostic_qualification</code>
     */
    public static final IndustryAgnosticQualification INDUSTRY_AGNOSTIC_QUALIFICATION = new IndustryAgnosticQualification();
    private static final long serialVersionUID = 1676900670;
    /**
     * The column <code>ess_ontology.industry_agnostic_qualification.id</code>.
     */
    public final TableField<IndustryAgnosticQualificationRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false).defaultValue(org.jooq.impl.DSL.field("nextval('ess_ontology.industry_agnostic_qualification_id_seq'::regclass)", org.jooq.impl.SQLDataType.INTEGER)), this, "");
    /**
     * The column <code>ess_ontology.industry_agnostic_qualification.position_id</code>.
     */
    public final TableField<IndustryAgnosticQualificationRecord, Integer> POSITION_ID = createField("position_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");
    /**
     * The column <code>ess_ontology.industry_agnostic_qualification.qualification_id</code>.
     */
    public final TableField<IndustryAgnosticQualificationRecord, Integer> QUALIFICATION_ID = createField("qualification_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * Create a <code>ess_ontology.industry_agnostic_qualification</code> table reference
     */
    public IndustryAgnosticQualification() {
        this(DSL.name("industry_agnostic_qualification"), null);
    }

    /**
     * Create an aliased <code>ess_ontology.industry_agnostic_qualification</code> table reference
     */
    public IndustryAgnosticQualification(String alias) {
        this(DSL.name(alias), INDUSTRY_AGNOSTIC_QUALIFICATION);
    }

    /**
     * Create an aliased <code>ess_ontology.industry_agnostic_qualification</code> table reference
     */
    public IndustryAgnosticQualification(Name alias) {
        this(alias, INDUSTRY_AGNOSTIC_QUALIFICATION);
    }

    private IndustryAgnosticQualification(Name alias, Table<IndustryAgnosticQualificationRecord> aliased) {
        this(alias, aliased, null);
    }

    private IndustryAgnosticQualification(Name alias, Table<IndustryAgnosticQualificationRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> IndustryAgnosticQualification(Table<O> child, ForeignKey<O, IndustryAgnosticQualificationRecord> key) {
        super(child, key, INDUSTRY_AGNOSTIC_QUALIFICATION);
    }

    /**
     * The class holding records for this type
     */
    @Override
    public Class<IndustryAgnosticQualificationRecord> getRecordType() {
        return IndustryAgnosticQualificationRecord.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return EssOntology.ESS_ONTOLOGY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.INDUSTRY_AGNOSTIC_QUALIFICATION_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<IndustryAgnosticQualificationRecord, Integer> getIdentity() {
        return Keys.IDENTITY_INDUSTRY_AGNOSTIC_QUALIFICATION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<IndustryAgnosticQualificationRecord> getPrimaryKey() {
        return Keys.INDUSTRY_AGNOSTIC_QUALIFICATION_PKEY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<IndustryAgnosticQualificationRecord>> getKeys() {
        return Arrays.<UniqueKey<IndustryAgnosticQualificationRecord>>asList(Keys.INDUSTRY_AGNOSTIC_QUALIFICATION_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<IndustryAgnosticQualificationRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<IndustryAgnosticQualificationRecord, ?>>asList(Keys.INDUSTRY_AGNOSTIC_QUALIFICATION__POSITION_ID, Keys.INDUSTRY_AGNOSTIC_QUALIFICATION__QUALIFICATION_ID);
    }

    public Position position() {
        return new Position(this, Keys.INDUSTRY_AGNOSTIC_QUALIFICATION__POSITION_ID);
    }

    public Qualification qualification() {
        return new Qualification(this, Keys.INDUSTRY_AGNOSTIC_QUALIFICATION__QUALIFICATION_ID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryAgnosticQualification as(String alias) {
        return new IndustryAgnosticQualification(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryAgnosticQualification as(Name alias) {
        return new IndustryAgnosticQualification(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public IndustryAgnosticQualification rename(String name) {
        return new IndustryAgnosticQualification(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public IndustryAgnosticQualification rename(Name name) {
        return new IndustryAgnosticQualification(name, null);
    }
}
