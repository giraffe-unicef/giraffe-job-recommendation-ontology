package za.co.giraffe.springboot_starter.model.ontology;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PositionFunction {

    @JsonProperty("position")
    private CommonOntologyObject position;
    @JsonProperty("function")
    private CommonOntologyObject function;
}
