package za.co.giraffe.springboot_starter.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class UpdateOntologyMapping {

    @JsonProperty("position_id")
    private List<Integer> positionIds;
    @JsonProperty("industry_id")
    private List<Integer> industryIds;
    @JsonProperty("section_ids")
    private List<Integer> inputIds;

    @JsonProperty("deleted_position_ids")
    private List<Integer> deletedPositionIds;
    @JsonProperty("deleted_industry_ids")
    private List<Integer> deletedIndustryIds;
    @JsonProperty("deleted_section_ids")
    private List<Integer> deletedInputIds;

    @JsonProperty("agnostic_industry")
    private boolean agnosticIndustry;
}
