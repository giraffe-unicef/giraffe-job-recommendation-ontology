package za.co.giraffe.springboot_starter.data.database.hstore;

import org.postgresql.util.HStoreConverter;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public final class HStoreValue extends TreeMap<String, String> {

    private static final long serialVersionUID = 1L;

    private static final Comparator<String> keyOrder = String::compareTo;

    public HStoreValue() {
        super(keyOrder);
    }

    public HStoreValue(final Map<String, String> values) {
        super(keyOrder);
        this.putAll(values);
    }

    @SuppressWarnings("unchecked")
    public static final za.co.giraffe.infrastructure.database.hstore.HStoreValue parse(final String value) {
        if (value == null) {
            return null;
        } else {
            return new za.co.giraffe.infrastructure.database.hstore.HStoreValue(HStoreConverter.fromString(value));
        }
    }

    public final za.co.giraffe.infrastructure.database.hstore.HStoreValue merge(final za.co.giraffe.infrastructure.database.hstore.HStoreValue other) {
        final za.co.giraffe.infrastructure.database.hstore.HStoreValue output = new za.co.giraffe.infrastructure.database.hstore.HStoreValue(this);
        output.putAll(other);
        return output;
    }

    @Override
    public final String toString() {
        return HStoreConverter.toString(this);
    }
}