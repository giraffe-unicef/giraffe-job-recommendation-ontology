package za.co.giraffe.springboot_starter.data.repository.ontology;

import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.*;
import za.co.giraffe.springboot_starter.model.ess.Function;
import za.co.giraffe.springboot_starter.model.ontology.Qualification;
import za.co.giraffe.springboot_starter.model.ontology.Skill;
import za.co.giraffe.springboot_starter.model.ontology.*;

import java.util.List;
import java.util.Optional;

public interface IOntologyDAO {
    List<Function> fetchFunctions();

    List<Position> fetchPositionsByFunctionId(Integer id);

    List<ProductsAndServices> fetchProductsServicesByIndustryPositionIds(Integer industryId, List<Integer> positionIds);

    List<Qualification> fetchQualificationsByIndustryPositionIds(Integer industryId, List<Integer> positionIds);

    List<Skill> fetchSkillsByIndustryPositionIds(Integer industryId, List<Integer> positionIds);

    /**
     * Ontology Editor
     */

    List<PositionWithAgnosticMapping> fetchAllPosition(Boolean agnosticIndustry, String section);

    List<PositionWithAgnosticMapping> fetchPositionsByIds(List<Integer> positionIds);

    List<ProductsAndServices> fetchAllProductsAndServices();

    List<Qualification> fetchAllQualifications();

    List<Skill> fetchAllSkills();

    void createSkill(za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Skill skill);

    void updateSkill(Integer id, String name);

    void createProductsAndServices(za.co.giraffe.infrastructure.entities.jooq.tables.pojos.ProductsServices productsServices);

    void updateProductsAndServices(Integer id, String name);

    void createQualifications(za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Qualification qualification);

    void updateQualifications(Integer id, String name);

    Position createPosition(Position position);

    void updatePosition(Integer id, String name);

    void createPositionAgnosticMapping(PositionAgnosticMapping positionAgnosticMapping);

    List<PositionFunctionMapping> fetchAllPositionFunctionMapping();

    Optional<PositionsFunction> checkIfMappingAlreadyExist(Integer positionId, Integer functionId);

    void createPositionsFunction(Integer positionId, Integer functionId);

    void deletePositionsFunction(Integer positionId, Integer functionId);

    List<OntologyMaster> fetchAllOntologyMapping();

    Optional<IndustryAndPositionsSkill> checkIfIndustryAndPositionsSkillExist(Integer industryId, Integer positionId, Integer skillId);

    Optional<IndustryAndPostionsProductsServices> checkIfIndustryAndPositionsProductsServicesExist(Integer industryId, Integer positionId, Integer productsServicesId);

    Optional<IndustryAndPostionsQualification> checkIfIndustryAndPositionsQualificationExist(Integer industryId, Integer positionId, Integer qualificationId);

    Optional<IndustryAgnosticSkill> checkIfIndustryAgnosticSkillExist(Integer positionId, Integer skillId);

    Optional<IndustryAgnosticProductsServices> checkIfIndustryAgnosticProductsServicesExist(Integer positionId, Integer productsServicesId);

    Optional<IndustryAgnosticQualification> checkIfIndustryAgnosticQualificationExist(Integer positionId, Integer qualificationId);

    void createIndustryAndPositionsSkill(Integer industryId, Integer positionId, Integer skillId);

    void createIndustryAndPostionsProductsServices(Integer industryId, Integer positionId, Integer productsServicesId);

    void createIndustryAndPostionsQualification(Integer industryId, Integer positionId, Integer qualificationId);

    void createIndustryAgnosticSkill(Integer positionId, Integer skillId);

    void createIndustryAgnosticProductsServices(Integer positionId, Integer productsServicesId);

    void createIndustryAgnosticQualification(Integer positionId, Integer qualificationId);


    List<IndustryPositionProductsServicesGroup> fetchIndustryPositionProductsServicesGroup();

    List<IndustryPositionQualificationsGroup> fetchIndustryPositionQualificationsGroup();

    List<IndustryPositionSkillsGroup> fetchIndustryPositionSkillsGroup();

    void deleteIndustryAndPostionsProductsServices(Integer industryId, Integer positionId, Integer productsServicesId);

    void deleteIndustryAndPostionsQualification(Integer industryId, Integer positionId, Integer qualificationId);

    void deleteIndustryAndPositionsSkill(Integer industryId, Integer positionId, Integer skillId);

    void deleteIndustryAgnosticProductsServices(Integer positionId, Integer productsServicesId);

    void deleteIndustryAgnosticQualification(Integer positionId, Integer qualificationId);

    void deleteIndustryAgnosticSkill(Integer positionId, Integer skillId);

    PositionWithAgnosticMapping getPositionById(Integer id);

    void updatePositionAgnosticMapping(Integer id, Boolean agnosticProductsAndServices, Boolean agnosticQualification, Boolean agnosticSkill);

}
