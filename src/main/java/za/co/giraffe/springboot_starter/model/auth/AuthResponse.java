package za.co.giraffe.springboot_starter.model.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthResponse {
    private boolean verified;
    @JsonProperty("is_admin")
    private boolean admin;
}
