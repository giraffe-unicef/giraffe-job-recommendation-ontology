package za.co.giraffe.springboot_starter.model.ontology;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class OntologyMapping {

    @JsonProperty("position_id")
    private Integer positionId;
    @JsonProperty("position_name")
    private String positionName;
    @JsonProperty("industry_id")
    private Integer industryId;
    @JsonProperty("industry_name")
    private String industryName;
    @JsonProperty("product_and_services")
    private List<CommonOntologyObject> productAndServices;
    @JsonProperty("qualifications")
    private List<CommonOntologyObject> qualifications;
    @JsonProperty("skills")
    private List<CommonOntologyObject> skills;

}
