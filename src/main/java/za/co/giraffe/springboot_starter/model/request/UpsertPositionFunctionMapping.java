package za.co.giraffe.springboot_starter.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class UpsertPositionFunctionMapping {

    @JsonProperty("function_id")
    private Integer functionId;
    @JsonProperty("position_ids")
    private List<Integer> positionIds;
    @JsonProperty("deleted_position_ids")
    private List<Integer> deletedPositionIds;
}
