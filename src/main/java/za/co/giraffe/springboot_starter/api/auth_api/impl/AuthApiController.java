package za.co.giraffe.springboot_starter.api.auth_api.impl;

import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import za.co.giraffe.springboot_starter.api.auth_api.AuthApi;
import za.co.giraffe.springboot_starter.configuration.auth.AuthConfig;
import za.co.giraffe.springboot_starter.model.auth.AuthRequest;
import za.co.giraffe.springboot_starter.model.auth.AuthResponse;

import javax.validation.Valid;

@Controller

public class AuthApiController implements AuthApi {
    private AuthConfig authConfig;

    @Autowired
    public AuthApiController(AuthConfig authConfig) {
        this.authConfig = authConfig;
    }

    @Override
    public ResponseEntity<AuthResponse> loginUser
            (@ApiParam @Valid @RequestBody AuthRequest authRequest) {
        boolean isAdmin = false;
        boolean loginPassed = authConfig.getPassword().equals(authRequest.getPassword()) &&
                authConfig.getUsername().equals(authRequest.getUsername());
        if (authRequest.getUsername().equals("admin")) {
            isAdmin = true;
        }
        return new ResponseEntity<>(new AuthResponse(loginPassed, isAdmin), HttpStatus.OK);
    }
}
