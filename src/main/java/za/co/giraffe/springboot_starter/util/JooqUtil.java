package za.co.giraffe.springboot_starter.util;

import org.jooq.Field;


public class JooqUtil {
    public static Field[] givePrefix(String prefix, Field[] fields) {
        Field[] out = new Field[fields.length];
        for (int i = 0; i < fields.length; i++) {
            out[i] = fields[i].as(prefix + "_" + fields[i].getName());
        }
        return out;
    }


}