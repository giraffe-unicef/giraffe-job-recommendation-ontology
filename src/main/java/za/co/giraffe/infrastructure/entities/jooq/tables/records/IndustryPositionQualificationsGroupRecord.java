/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables.records;


import org.jooq.Field;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.TableRecordImpl;
import za.co.giraffe.infrastructure.entities.jooq.tables.IndustryPositionQualificationsGroup;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.jooq.org",
                "jOOQ version:3.11.9"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class IndustryPositionQualificationsGroupRecord extends TableRecordImpl<IndustryPositionQualificationsGroupRecord> implements Record4<Integer[], Integer[], Integer[], Boolean> {

    private static final long serialVersionUID = 891495868;

    /**
     * Create a detached IndustryPositionQualificationsGroupRecord
     */
    public IndustryPositionQualificationsGroupRecord() {
        super(IndustryPositionQualificationsGroup.INDUSTRY_POSITION_QUALIFICATIONS_GROUP);
    }

    /**
     * Create a detached, initialised IndustryPositionQualificationsGroupRecord
     */
    public IndustryPositionQualificationsGroupRecord(Integer[] positionIds, Integer[] industryIds, Integer[] qualificationIds, Boolean agnosticQualification) {
        super(IndustryPositionQualificationsGroup.INDUSTRY_POSITION_QUALIFICATIONS_GROUP);

        set(0, positionIds);
        set(1, industryIds);
        set(2, qualificationIds);
        set(3, agnosticQualification);
    }

    /**
     * Getter for <code>ess_ontology.industry_position_qualifications_group.position_ids</code>.
     */
    public Integer[] getPositionIds() {
        return (Integer[]) get(0);
    }

    /**
     * Setter for <code>ess_ontology.industry_position_qualifications_group.position_ids</code>.
     */
    public void setPositionIds(Integer... value) {
        set(0, value);
    }

    /**
     * Getter for <code>ess_ontology.industry_position_qualifications_group.industry_ids</code>.
     */
    public Integer[] getIndustryIds() {
        return (Integer[]) get(1);
    }

    /**
     * Setter for <code>ess_ontology.industry_position_qualifications_group.industry_ids</code>.
     */
    public void setIndustryIds(Integer... value) {
        set(1, value);
    }

    /**
     * Getter for <code>ess_ontology.industry_position_qualifications_group.qualification_ids</code>.
     */
    public Integer[] getQualificationIds() {
        return (Integer[]) get(2);
    }

    /**
     * Setter for <code>ess_ontology.industry_position_qualifications_group.qualification_ids</code>.
     */
    public void setQualificationIds(Integer... value) {
        set(2, value);
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    /**
     * Getter for <code>ess_ontology.industry_position_qualifications_group.agnostic_qualification</code>.
     */
    public Boolean getAgnosticQualification() {
        return (Boolean) get(3);
    }

    /**
     * Setter for <code>ess_ontology.industry_position_qualifications_group.agnostic_qualification</code>.
     */
    public void setAgnosticQualification(Boolean value) {
        set(3, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Integer[], Integer[], Integer[], Boolean> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Integer[], Integer[], Integer[], Boolean> valuesRow() {
        return (Row4) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer[]> field1() {
        return IndustryPositionQualificationsGroup.INDUSTRY_POSITION_QUALIFICATIONS_GROUP.POSITION_IDS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer[]> field2() {
        return IndustryPositionQualificationsGroup.INDUSTRY_POSITION_QUALIFICATIONS_GROUP.INDUSTRY_IDS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer[]> field3() {
        return IndustryPositionQualificationsGroup.INDUSTRY_POSITION_QUALIFICATIONS_GROUP.QUALIFICATION_IDS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Boolean> field4() {
        return IndustryPositionQualificationsGroup.INDUSTRY_POSITION_QUALIFICATIONS_GROUP.AGNOSTIC_QUALIFICATION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] component1() {
        return getPositionIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] component2() {
        return getIndustryIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] component3() {
        return getQualificationIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean component4() {
        return getAgnosticQualification();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] value1() {
        return getPositionIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] value2() {
        return getIndustryIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer[] value3() {
        return getQualificationIds();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean value4() {
        return getAgnosticQualification();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryPositionQualificationsGroupRecord value1(Integer... value) {
        setPositionIds(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryPositionQualificationsGroupRecord value2(Integer... value) {
        setIndustryIds(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryPositionQualificationsGroupRecord value3(Integer... value) {
        setQualificationIds(value);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryPositionQualificationsGroupRecord value4(Boolean value) {
        setAgnosticQualification(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryPositionQualificationsGroupRecord values(Integer[] value1, Integer[] value2, Integer[] value3, Boolean value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }
}
