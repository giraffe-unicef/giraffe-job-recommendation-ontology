package za.co.giraffe.springboot_starter.model.ontology;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class PositionFunctionMapping {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("description")
    private String description;
    @JsonProperty("position")
    private List<Position> position = new ArrayList<>();

    public PositionFunctionMapping(Integer id, String description, List<CommonOntologyObject> position) {
        this.id = id;
        this.description = description;
        this.position = position.parallelStream()
                .map(fun -> new Position(fun.getId(), fun.getDescription()))
                .collect(Collectors.toList());
    }
}
