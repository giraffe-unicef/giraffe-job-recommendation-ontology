package za.co.giraffe.infrastructure.database;

import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseInf {
    Connection getConnection() throws SQLException;
}
