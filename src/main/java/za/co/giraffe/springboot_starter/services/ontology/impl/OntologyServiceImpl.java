package za.co.giraffe.springboot_starter.services.ontology.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.*;
import za.co.giraffe.springboot_starter.data.repository.ess_posts.IEssApprovePostFormOptionsDAO;
import za.co.giraffe.springboot_starter.data.repository.ontology.IOntologyDAO;
import za.co.giraffe.springboot_starter.model.ess.Function;
import za.co.giraffe.springboot_starter.model.ess.Industry;
import za.co.giraffe.springboot_starter.model.ontology.Qualification;
import za.co.giraffe.springboot_starter.model.ontology.Skill;
import za.co.giraffe.springboot_starter.model.ontology.*;
import za.co.giraffe.springboot_starter.model.request.*;
import za.co.giraffe.springboot_starter.services.ontology.IOntologyService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OntologyServiceImpl implements IOntologyService {
    private IOntologyDAO iOntologyDAO;
    private IEssApprovePostFormOptionsDAO iEssApprovePostFormOptionsDAO;

    @Autowired
    public OntologyServiceImpl(IOntologyDAO iOntologyDAO, IEssApprovePostFormOptionsDAO iEssApprovePostFormOptionsDAO) {
        this.iOntologyDAO = iOntologyDAO;
        this.iEssApprovePostFormOptionsDAO = iEssApprovePostFormOptionsDAO;
    }

    @Override
    public List<Function> handleFetchFunctions() {
        return iOntologyDAO.fetchFunctions();
    }

    @Override
    public List<Position> handleFetchPositionsByFunctionId(Integer id) {
        return iOntologyDAO.fetchPositionsByFunctionId(id);
    }

    @Override
    public List<ProductsAndServices> handleFetchProductsAndServicesByIndustryPositionIds(Integer industryId,
                                                                                         List<Integer> positionIds) {
        return iOntologyDAO.fetchProductsServicesByIndustryPositionIds(industryId, positionIds);
    }

    @Override
    public List<Qualification> handleFetchQualificationsByIndustryPositionIds(Integer industryId,
                                                                              List<Integer> positionIds) {
        return iOntologyDAO.fetchQualificationsByIndustryPositionIds(industryId, positionIds);
    }

    @Override
    public List<Skill> handleFetchSkillsByIndustryPositionIds(Integer industryId,
                                                              List<Integer> positionIds) {
        return iOntologyDAO.fetchSkillsByIndustryPositionIds(industryId, positionIds);
    }

    @Override
    public List<Industry> handleFetchIndustries() {
        return iEssApprovePostFormOptionsDAO.fetchIndustries();
    }


    /**
     * Ontology Editor Service
     */


    @Override
    public List<PositionWithAgnosticMapping> handleFetchAllPosition(Boolean agnosticIndustry, String section) {
        //Boolean agnostic = agnosticIndustry != null ? Boolean.valueOf(agnosticIndustry) : null;
        return iOntologyDAO.fetchAllPosition(agnosticIndustry, section);
    }

    @Override
    public List<ProductsAndServices> handleFetchAllProductsAndServices() {
        return iOntologyDAO.fetchAllProductsAndServices();
    }

    @Override
    public List<Qualification> handleFetchAllQualifications() {
        return iOntologyDAO.fetchAllQualifications();
    }

    @Override
    public List<Skill> handleFetchAllSkills() {
        return iOntologyDAO.fetchAllSkills();
    }

    @Override
    public PositionWithAgnosticMapping handleCreatePositionWithAgnosticMapping(PositionRequest positionRequest) {
        Position position = new Position();
        position.setDescription(positionRequest.getName());
        position = iOntologyDAO.createPosition(position);
        PositionAgnosticMapping positionAgnosticMapping = new PositionAgnosticMapping();
        positionAgnosticMapping.setPositionId(position.getId());
        positionAgnosticMapping.setAgnosticProductsServices(positionRequest.getAgnosticProductsAndServices());
        positionAgnosticMapping.setAgnosticQualification(positionRequest.getAgnosticQualification());
        positionAgnosticMapping.setAgnosticSkill(positionRequest.getAgnosticSkill());
        iOntologyDAO.createPositionAgnosticMapping(positionAgnosticMapping);
        return new PositionWithAgnosticMapping(position.getId(),
                position.getDescription(),
                positionRequest.getAgnosticProductsAndServices(),
                positionRequest.getAgnosticQualification(),
                positionRequest.getAgnosticSkill());
    }

    @Override
    public void handleUpsertProductsAndServices(Integer id, OntologyEditorRequest ontologyEditorRequest) {
        if (id != null && id != 0) {
            iOntologyDAO.updateProductsAndServices(id, ontologyEditorRequest.getName());
        } else {
            ProductsServices productsServices = new ProductsServices();
            productsServices.setName(ontologyEditorRequest.getName());
            iOntologyDAO.createProductsAndServices(productsServices);
        }
    }

    @Override
    public void handleUpsertQualifications(Integer id, OntologyEditorRequest ontologyEditorRequest) {
        if (id != null && id != 0) {
            iOntologyDAO.updateQualifications(id, ontologyEditorRequest.getName());
        } else {
            za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Qualification qualification
                    = new za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Qualification();
            qualification.setName(ontologyEditorRequest.getName());
            iOntologyDAO.createQualifications(qualification);
        }
    }

    @Override
    public void handleUpsertSkills(Integer id, OntologyEditorRequest ontologyEditorRequest) {
        if (id != null && id != 0) {
            iOntologyDAO.updateSkill(id, ontologyEditorRequest.getName());
        } else {
            za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Skill skill
                    = new za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Skill();
            skill.setName(ontologyEditorRequest.getName());
            iOntologyDAO.createSkill(skill);
        }
    }

    @Override
    public void handleUpdatePosition(Integer id, OntologyEditorRequest ontologyEditorRequest) {
        if (id != null && id != 0) {
            iOntologyDAO.updatePosition(id, ontologyEditorRequest.getName());
        }
    }

    @Override
    public List<PositionFunctionMapping> handleFetchAllPositionFunctionMapping() {
        return iOntologyDAO.fetchAllPositionFunctionMapping();
    }

    @Override
    public void handleUpsertPositionFunctionMapping(UpsertPositionFunctionMapping upsertPositionFunctionMapping) {
        if (upsertPositionFunctionMapping.getFunctionId() != null) {
            Integer functionId = upsertPositionFunctionMapping.getFunctionId();
            // to create mapping if not exist
            if (upsertPositionFunctionMapping.getPositionIds() != null && !upsertPositionFunctionMapping.getPositionIds().isEmpty()) {
                upsertPositionFunctionMapping.getPositionIds().stream().forEach(positionId -> {
                    Optional<PositionsFunction> optionalPositionsFunction = iOntologyDAO.checkIfMappingAlreadyExist(positionId, functionId);
                    if (!optionalPositionsFunction.isPresent()) {
                        iOntologyDAO.createPositionsFunction(positionId, functionId);
                    }
                });
            }

            // to delete mapping
            if (upsertPositionFunctionMapping.getDeletedPositionIds() != null && !upsertPositionFunctionMapping.getDeletedPositionIds().isEmpty()) {
                upsertPositionFunctionMapping.getDeletedPositionIds().stream().forEach(positionId -> {
                    iOntologyDAO.deletePositionsFunction(positionId, functionId);
                });
            }

        }
    }

    @Override
    public List<OntologyMaster> handleFetchAllOntologyMapping() {
        List<OntologyMaster> ontologyMasters = iOntologyDAO.fetchAllOntologyMapping();
        return ontologyMasters;
    }

    @Override
    public List<IndustryPositionProductsServicesGroup> handleFetchIndustryPositionProductsServicesGroup() {
        return iOntologyDAO.fetchIndustryPositionProductsServicesGroup();
    }

    @Override
    public List<IndustryPositionQualificationsGroup> handleFetchIndustryPositionQualificationsGroup() {
        return iOntologyDAO.fetchIndustryPositionQualificationsGroup();
    }

    @Override
    public List<IndustryPositionSkillsGroup> handleFetchIndustryPositionSkillsGroup() {
        return iOntologyDAO.fetchIndustryPositionSkillsGroup();
    }

    private Map<Integer, PositionWithAgnosticMapping> getPositionMap(List<Integer> positionIds) {
        return iOntologyDAO.fetchPositionsByIds(positionIds)
                .stream().collect(Collectors.toMap(PositionWithAgnosticMapping::getId, pos -> pos));
    }

    @Override
    public void handleUpsertIndustryPositionProductsServicesGroupMapping(UpdateOntologyMapping updateOntologyMasterMapping) throws Exception {
        Map<Integer, PositionWithAgnosticMapping> positionMap = getPositionMap(updateOntologyMasterMapping.getPositionIds());
        SectionEnum section = SectionEnum.PRODUCT_SERVICE;
        if (updateOntologyMasterMapping.isAgnosticIndustry()) {
            // inset into agnostic mapping table
            updateOntologyMasterMapping.getPositionIds().stream().filter(positionId -> positionId != null).forEach(positionId -> {
                Boolean agnosticMapping = positionMap.get(positionId).getAgnosticProductsAndServices();
                if (agnosticMapping == null) {
                    upsertPositionAgnosticMapping(positionId, section, true);
                }
                updateOntologyMasterMapping.getInputIds().stream().filter(productServiceId -> productServiceId != null).forEach(productServiceId -> {
                    Optional<IndustryAgnosticProductsServices> optionalIndustryAgnosticProductsServices =
                            iOntologyDAO.checkIfIndustryAgnosticProductsServicesExist(positionId, productServiceId);
                    if (!optionalIndustryAgnosticProductsServices.isPresent()) {
                        iOntologyDAO.createIndustryAgnosticProductsServices(positionId, productServiceId);
                    }
                });
            });
        } else {
            // inset into normal mapping table
            updateOntologyMasterMapping.getPositionIds().stream().filter(positionId -> positionId != null).forEach(positionId -> {
                Boolean agnosticMapping = positionMap.get(positionId).getAgnosticProductsAndServices();
                if (agnosticMapping == null) {
                    upsertPositionAgnosticMapping(positionId, section, false);
                }
                updateOntologyMasterMapping.getIndustryIds().stream().filter(industryId -> industryId != null).forEach(industryId -> {
                    updateOntologyMasterMapping.getInputIds().stream().filter(productServiceId -> productServiceId != null).forEach(productServiceId -> {
                        Optional<IndustryAndPostionsProductsServices> optionalIndustryAndPositionsProductsServices =
                                iOntologyDAO.checkIfIndustryAndPositionsProductsServicesExist(industryId, positionId, productServiceId);
                        if (!optionalIndustryAndPositionsProductsServices.isPresent()) {
                            iOntologyDAO.createIndustryAndPostionsProductsServices(industryId, positionId, productServiceId);
                        }
                    });
                });
            });
        }
        // delete logic
        deleteOntologyMapping(updateOntologyMasterMapping, section);
    }

    private void deleteOntologyMapping(UpdateOntologyMapping updateOntologyMasterMapping, SectionEnum section) {
        if (updateOntologyMasterMapping.isAgnosticIndustry()) {
            deleteIndustryAgnosticSectionGroupMapping(updateOntologyMasterMapping, section);
        } else {
            deleteIndustryAndPostionsSectionGroupMapping(updateOntologyMasterMapping, section);
        }

    }

    private void deleteIndustryAgnosticSectionGroupMapping(UpdateOntologyMapping updateOntologyMasterMapping, SectionEnum section) {
        // DELETE LOGIC For position mapping
        if (updateOntologyMasterMapping.getDeletedPositionIds() != null && !updateOntologyMasterMapping.getPositionIds().isEmpty()) {
            updateOntologyMasterMapping.getDeletedPositionIds().stream().filter(positionId -> positionId != null).forEach(deletedPositionId -> {
                updateOntologyMasterMapping.getInputIds().stream().filter(sectionId -> sectionId != null).forEach(sectionId -> {
                    deleteIndustryAgnosticSectionSingleEntry(deletedPositionId, sectionId, section);
                });
            });
        }
        // DELETE LOGIC For productServiceId mapping
        if (updateOntologyMasterMapping.getDeletedInputIds() != null && !updateOntologyMasterMapping.getDeletedInputIds().isEmpty()) {
            updateOntologyMasterMapping.getDeletedInputIds().stream().filter(sectionId -> sectionId != null).forEach(deletedSectionId -> {
                updateOntologyMasterMapping.getPositionIds().stream().filter(positionId -> positionId != null).forEach(positionId -> {
                    deleteIndustryAgnosticSectionSingleEntry(positionId, deletedSectionId, section);
                });
            });
        }
    }

    private void deleteIndustryAndPostionsSectionGroupMapping(UpdateOntologyMapping updateOntologyMasterMapping, SectionEnum section) {
        // DELETE LOGIC For position mapping
        if (updateOntologyMasterMapping.getDeletedPositionIds() != null && !updateOntologyMasterMapping.getPositionIds().isEmpty()) {
            updateOntologyMasterMapping.getDeletedPositionIds().stream().filter(positionId -> positionId != null).forEach(deletedPositionId -> {
                updateOntologyMasterMapping.getIndustryIds().stream().filter(industryId -> industryId != null).forEach(industryId -> {
                    updateOntologyMasterMapping.getInputIds().stream().filter(sectionId -> sectionId != null).forEach(sectionId -> {
                        deleteIndustryAndPositionsSectionSingleEntry(industryId, deletedPositionId, sectionId, section);
                    });
                });
            });
        }
        // DELETE LOGIC For Industry mapping
        if (updateOntologyMasterMapping.getDeletedIndustryIds() != null && !updateOntologyMasterMapping.getDeletedIndustryIds().isEmpty()) {
            updateOntologyMasterMapping.getDeletedIndustryIds().stream().filter(industryId -> industryId != null).forEach(deletedIndustryId -> {
                updateOntologyMasterMapping.getPositionIds().stream().filter(positionId -> positionId != null).forEach(positionId -> {
                    updateOntologyMasterMapping.getInputIds().stream().filter(sectionId -> sectionId != null).forEach(sectionId -> {
                        iOntologyDAO.deleteIndustryAndPostionsProductsServices(deletedIndustryId, positionId, sectionId);
                        deleteIndustryAndPositionsSectionSingleEntry(deletedIndustryId, positionId, sectionId, section);
                    });
                });
            });
        }
        // DELETE LOGIC For section mapping
        if (updateOntologyMasterMapping.getDeletedInputIds() != null && !updateOntologyMasterMapping.getDeletedInputIds().isEmpty()) {
            updateOntologyMasterMapping.getDeletedInputIds().stream().filter(deletedSectionId -> deletedSectionId != null).forEach(deletedSectionId -> {
                updateOntologyMasterMapping.getPositionIds().stream().filter(positionId -> positionId != null).forEach(positionId -> {
                    updateOntologyMasterMapping.getIndustryIds().stream().filter(industryId -> industryId != null).forEach(industryId -> {
                        deleteIndustryAndPositionsSectionSingleEntry(industryId, positionId, deletedSectionId, section);
                    });
                });
            });
        }
    }

    private void deleteIndustryAndPositionsSectionSingleEntry(Integer industryId, Integer positionId, Integer sectionId, SectionEnum section) {
        if (section.equals(SectionEnum.PRODUCT_SERVICE)) {
            iOntologyDAO.deleteIndustryAndPostionsProductsServices(industryId, positionId, sectionId);
        } else if (section.equals(SectionEnum.SKILL)) {
            iOntologyDAO.deleteIndustryAndPositionsSkill(industryId, positionId, sectionId);
        } else if (section.equals(SectionEnum.QUALIFICATION)) {
            iOntologyDAO.deleteIndustryAndPostionsQualification(industryId, positionId, sectionId);
        }
    }

    private void deleteIndustryAgnosticSectionSingleEntry(Integer positionId, Integer sectionId, SectionEnum section) {
        if (section.equals(SectionEnum.PRODUCT_SERVICE)) {
            iOntologyDAO.deleteIndustryAgnosticProductsServices(positionId, sectionId);
        } else if (section.equals(SectionEnum.SKILL)) {
            iOntologyDAO.deleteIndustryAgnosticSkill(positionId, sectionId);
        } else if (section.equals(SectionEnum.QUALIFICATION)) {
            iOntologyDAO.deleteIndustryAgnosticQualification(positionId, sectionId);
        }
    }


    @Override
    public void handleUpsertIndustryPositionQualificationsGroupMapping(UpdateOntologyMapping updateOntologyMasterMapping) throws Exception {
        Map<Integer, PositionWithAgnosticMapping> positionMap = getPositionMap(updateOntologyMasterMapping.getPositionIds());
        SectionEnum section = SectionEnum.QUALIFICATION;
        if (updateOntologyMasterMapping.isAgnosticIndustry()) {
            // inset into agnostic mapping table
            updateOntologyMasterMapping.getPositionIds().stream().filter(positionId -> positionId != null).forEach(positionId -> {
                Boolean agnosticMapping = positionMap.get(positionId).getAgnosticQualification();
                if (agnosticMapping == null) {
                    upsertPositionAgnosticMapping(positionId, section, true);
                }
                updateOntologyMasterMapping.getInputIds().stream().filter(qualificationId -> qualificationId != null).forEach(qualificationId -> {
                    Optional<IndustryAgnosticQualification> optionalIndustryAgnosticQualification =
                            iOntologyDAO.checkIfIndustryAgnosticQualificationExist(positionId, qualificationId);
                    if (!optionalIndustryAgnosticQualification.isPresent()) {
                        iOntologyDAO.createIndustryAgnosticQualification(positionId, qualificationId);
                    }
                });
            });

        } else {
            // inset into normal mapping table
            updateOntologyMasterMapping.getPositionIds().stream().filter(positionId -> positionId != null).forEach(positionId -> {
                Boolean agnosticMapping = positionMap.get(positionId).getAgnosticQualification();
                if (agnosticMapping == null) {
                    upsertPositionAgnosticMapping(positionId, section, false);
                }
                updateOntologyMasterMapping.getIndustryIds().stream().filter(industryId -> industryId != null).forEach(industryId -> {
                    updateOntologyMasterMapping.getInputIds().stream().filter(qualificationId -> qualificationId != null).forEach(qualificationId -> {
                        Optional<IndustryAndPostionsQualification> optionalIndustryAndPositionsQualification =
                                iOntologyDAO.checkIfIndustryAndPositionsQualificationExist(industryId, positionId, qualificationId);
                        if (!optionalIndustryAndPositionsQualification.isPresent()) {
                            iOntologyDAO.createIndustryAndPostionsQualification(industryId, positionId, qualificationId);
                        }
                    });
                });
            });
        }
        // delete logic
        deleteOntologyMapping(updateOntologyMasterMapping, section);
    }

    @Override
    public void handleUpsertIndustryPositionSkillsGroupMapping(UpdateOntologyMapping updateOntologyMasterMapping) throws Exception {
        SectionEnum section = SectionEnum.SKILL;
        Map<Integer, PositionWithAgnosticMapping> positionMap = getPositionMap(updateOntologyMasterMapping.getPositionIds());
        if (updateOntologyMasterMapping.isAgnosticIndustry()) {
            // inset into agnostic mapping table
            updateOntologyMasterMapping.getPositionIds().stream().filter(positionId -> positionId != null).forEach(positionId -> {
                Boolean agnosticMapping = positionMap.get(positionId).getAgnosticSkill();
                if (agnosticMapping == null) {
                    upsertPositionAgnosticMapping(positionId, section, true);
                }
                updateOntologyMasterMapping.getInputIds().stream().filter(skillId -> skillId != null).forEach(skillId -> {
                    Optional<IndustryAgnosticSkill> optionalIndustryAgnosticSkill = iOntologyDAO.checkIfIndustryAgnosticSkillExist(positionId, skillId);
                    if (!optionalIndustryAgnosticSkill.isPresent()) {
                        iOntologyDAO.createIndustryAgnosticSkill(positionId, skillId);
                    }
                });
            });
        } else {
            // inset into normal mapping table
            updateOntologyMasterMapping.getPositionIds().stream().filter(positionId -> positionId != null).forEach(positionId -> {
                Boolean agnosticMapping = positionMap.get(positionId).getAgnosticSkill();
                if (agnosticMapping == null) {
                    upsertPositionAgnosticMapping(positionId, section, false);
                }
                updateOntologyMasterMapping.getIndustryIds().stream().filter(industryId -> industryId != null).forEach(industryId -> {
                    updateOntologyMasterMapping.getInputIds().stream().filter(skillId -> skillId != null).forEach(skillId -> {
                        Optional<IndustryAndPositionsSkill> optionalIndustryAndPositionsSkill =
                                iOntologyDAO.checkIfIndustryAndPositionsSkillExist(industryId, positionId, skillId);
                        if (!optionalIndustryAndPositionsSkill.isPresent()) {
                            iOntologyDAO.createIndustryAndPositionsSkill(industryId, positionId, skillId);
                        }
                    });
                });
            });
        }
        // delete logic
        deleteOntologyMapping(updateOntologyMasterMapping, section);
    }

    @Override
    public void handleCreateIndustryPositionProductAndServiceGroupMapping(CreateOntologyMapping createOntologyMapping) throws Exception {
        Integer positionId = createOntologyMapping.getPositionId();
        PositionWithAgnosticMapping positionWithAgnosticMapping = iOntologyDAO.getPositionById(positionId);
        boolean agnostic = createOntologyMapping.isAgnosticIndustry();
        if (positionWithAgnosticMapping.getAgnosticProductsAndServices() != null &&
                !positionWithAgnosticMapping.getAgnosticProductsAndServices().equals(agnostic)) {
            throw new Exception("Industry agnostic mapping mismatch");
        }
        if (!agnostic && (createOntologyMapping.getIndustryIds() == null || createOntologyMapping.getIndustryIds().isEmpty())) {
            throw new Exception("Industry is required");
        }
       /* if(agnostic && createOntologyMapping.getIndustryIds() != null &&
                !createOntologyMapping.getIndustryIds().isEmpty()){
            agnostic = false;
        }*/
        if (positionWithAgnosticMapping.getAgnosticProductsAndServices() == null) {
            // todo : call generic function
            iOntologyDAO.updatePositionAgnosticMapping(positionId, agnostic, null, null);
        }
        // call upsertmethod
        UpdateOntologyMapping updateOntologyMasterMapping = new UpdateOntologyMapping();
        updateOntologyMasterMapping.setPositionIds(Arrays.asList(positionId));
        updateOntologyMasterMapping.setIndustryIds(createOntologyMapping.getIndustryIds());
        updateOntologyMasterMapping.setInputIds(createOntologyMapping.getInputIds());
        updateOntologyMasterMapping.setAgnosticIndustry(agnostic);
        handleUpsertIndustryPositionProductsServicesGroupMapping(updateOntologyMasterMapping);
    }

    @Override
    public void handleCreateIndustryPositionQualificationMapping(CreateOntologyMapping createOntologyMapping) throws Exception {
        Integer positionId = createOntologyMapping.getPositionId();
        PositionWithAgnosticMapping positionWithAgnosticMapping = iOntologyDAO.getPositionById(positionId);
        boolean agnostic = createOntologyMapping.isAgnosticIndustry();
        if (positionWithAgnosticMapping.getAgnosticQualification() != null &&
                !positionWithAgnosticMapping.getAgnosticQualification().equals(agnostic)) {
            throw new Exception("Industry agnostic mapping mismatch");
        }
        if (!agnostic && (createOntologyMapping.getIndustryIds() == null || createOntologyMapping.getIndustryIds().isEmpty())) {
            throw new Exception("Industry is required");
        }
        /*if(agnostic && createOntologyMapping.getIndustryIds() != null &&
                !createOntologyMapping.getIndustryIds().isEmpty()){
            agnostic = false;
        }*/
        if (positionWithAgnosticMapping.getAgnosticProductsAndServices() == null) {
            // todo : call generic function
            iOntologyDAO.updatePositionAgnosticMapping(positionId, null, agnostic, null);
        }
        // call upsert method
        UpdateOntologyMapping updateOntologyMasterMapping = new UpdateOntologyMapping();
        updateOntologyMasterMapping.setPositionIds(Arrays.asList(positionId));
        updateOntologyMasterMapping.setIndustryIds(createOntologyMapping.getIndustryIds());
        updateOntologyMasterMapping.setInputIds(createOntologyMapping.getInputIds());
        updateOntologyMasterMapping.setAgnosticIndustry(agnostic);
        handleUpsertIndustryPositionQualificationsGroupMapping(updateOntologyMasterMapping);
    }

    @Override
    public void handleCreateIndustryPositionSkillsGroupMapping(CreateOntologyMapping createOntologyMapping) throws Exception {
        Integer positionId = createOntologyMapping.getPositionId();
        PositionWithAgnosticMapping positionWithAgnosticMapping = iOntologyDAO.getPositionById(positionId);
        boolean agnostic = createOntologyMapping.isAgnosticIndustry();
        if (positionWithAgnosticMapping.getAgnosticSkill() != null &&
                !positionWithAgnosticMapping.getAgnosticSkill().equals(agnostic)) {
            throw new Exception("Industry agnostic mapping mismatch");
        }
        if (!agnostic && (createOntologyMapping.getIndustryIds() == null || createOntologyMapping.getIndustryIds().isEmpty())) {
            throw new Exception("Industry is required");
        }
        /*if(agnostic && createOntologyMapping.getIndustryIds() != null &&
                !createOntologyMapping.getIndustryIds().isEmpty()){
            agnostic = false;
        }*/
        if (positionWithAgnosticMapping.getAgnosticProductsAndServices() == null) {
            // todo : call generic function
            iOntologyDAO.updatePositionAgnosticMapping(positionId, null, null, agnostic);
        }
        // call upsert method
        UpdateOntologyMapping updateOntologyMasterMapping = new UpdateOntologyMapping();
        updateOntologyMasterMapping.setPositionIds(Arrays.asList(positionId));
        updateOntologyMasterMapping.setIndustryIds(createOntologyMapping.getIndustryIds());
        updateOntologyMasterMapping.setInputIds(createOntologyMapping.getInputIds());
        updateOntologyMasterMapping.setAgnosticIndustry(agnostic);
        handleUpsertIndustryPositionSkillsGroupMapping(updateOntologyMasterMapping);
    }

    // TODO: this needs to be updated
    private void upsertPositionAgnosticMapping(Integer positionId, SectionEnum section, boolean agnostic) {
        if (section.equals(SectionEnum.PRODUCT_SERVICE)) {
            iOntologyDAO.updatePositionAgnosticMapping(positionId, agnostic, null, null);
        } else if (section.equals(SectionEnum.SKILL)) {
            iOntologyDAO.updatePositionAgnosticMapping(positionId, null, null, agnostic);
        } else if (section.equals(SectionEnum.QUALIFICATION)) {
            iOntologyDAO.updatePositionAgnosticMapping(positionId, null, agnostic, null);
        }
    }
}

