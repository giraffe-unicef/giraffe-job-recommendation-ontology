package za.co.giraffe.springboot_starter.model.ontology;

public enum SectionEnum {
    PRODUCT_SERVICE,
    SKILL,
    QUALIFICATION
}
