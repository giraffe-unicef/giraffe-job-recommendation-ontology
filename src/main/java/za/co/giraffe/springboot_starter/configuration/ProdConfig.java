package za.co.giraffe.springboot_starter.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("prod")
public class ProdConfig {
    private String ip;
}
