package za.co.giraffe.springboot_starter.model.ess;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Function {
    private Integer id;

    @JsonProperty("name")
    private String description;
}
