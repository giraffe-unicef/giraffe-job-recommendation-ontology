package za.co.giraffe.springboot_starter.model.api_util;

public enum Direction {
    ASCENDING("ASC"),
    DESCENDING("DESC");

    private final String directionCode;

    Direction(String direction) {
        this.directionCode = direction;
    }

    public String getDirectionCode() {
        return this.directionCode;
    }
}