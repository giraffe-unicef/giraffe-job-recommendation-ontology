package za.co.giraffe.springboot_starter.api;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-02-26T08:11:33.670Z[GMT]")
public class NotFoundException extends za.co.giraffe.springboot_starter.api.ApiException {
    private int code;

    public NotFoundException(int code, String msg) {
        super(code, msg);
        this.code = code;
    }
}
