package za.co.giraffe.springboot_starter.configuration.auth;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("auth")
public class AuthConfig {
    private String username;
    private String password;
}
