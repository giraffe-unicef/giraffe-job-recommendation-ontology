/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables.daos;


import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;
import za.co.giraffe.infrastructure.entities.jooq.tables.IndustryAndPostionsQualification;
import za.co.giraffe.infrastructure.entities.jooq.tables.records.IndustryAndPostionsQualificationRecord;

import javax.annotation.Generated;
import java.util.List;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.jooq.org",
                "jOOQ version:3.11.9"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class IndustryAndPostionsQualificationDao extends DAOImpl<IndustryAndPostionsQualificationRecord, za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryAndPostionsQualification, Integer> {

    /**
     * Create a new IndustryAndPostionsQualificationDao without any configuration
     */
    public IndustryAndPostionsQualificationDao() {
        super(IndustryAndPostionsQualification.INDUSTRY_AND_POSTIONS_QUALIFICATION, za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryAndPostionsQualification.class);
    }

    /**
     * Create a new IndustryAndPostionsQualificationDao with an attached configuration
     */
    public IndustryAndPostionsQualificationDao(Configuration configuration) {
        super(IndustryAndPostionsQualification.INDUSTRY_AND_POSTIONS_QUALIFICATION, za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryAndPostionsQualification.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer getId(za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryAndPostionsQualification object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryAndPostionsQualification> fetchById(Integer... values) {
        return fetch(IndustryAndPostionsQualification.INDUSTRY_AND_POSTIONS_QUALIFICATION.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryAndPostionsQualification fetchOneById(Integer value) {
        return fetchOne(IndustryAndPostionsQualification.INDUSTRY_AND_POSTIONS_QUALIFICATION.ID, value);
    }

    /**
     * Fetch records that have <code>industry_id IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryAndPostionsQualification> fetchByIndustryId(Integer... values) {
        return fetch(IndustryAndPostionsQualification.INDUSTRY_AND_POSTIONS_QUALIFICATION.INDUSTRY_ID, values);
    }

    /**
     * Fetch records that have <code>position_id IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryAndPostionsQualification> fetchByPositionId(Integer... values) {
        return fetch(IndustryAndPostionsQualification.INDUSTRY_AND_POSTIONS_QUALIFICATION.POSITION_ID, values);
    }

    /**
     * Fetch records that have <code>qualification_id IN (values)</code>
     */
    public List<za.co.giraffe.infrastructure.entities.jooq.tables.pojos.IndustryAndPostionsQualification> fetchByQualificationId(Integer... values) {
        return fetch(IndustryAndPostionsQualification.INDUSTRY_AND_POSTIONS_QUALIFICATION.QUALIFICATION_ID, values);
    }
}
