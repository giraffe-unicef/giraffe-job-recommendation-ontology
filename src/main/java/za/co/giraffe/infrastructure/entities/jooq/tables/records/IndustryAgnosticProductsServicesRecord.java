/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables.records;


import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;
import za.co.giraffe.infrastructure.entities.jooq.tables.IndustryAgnosticProductsServices;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.jooq.org",
                "jOOQ version:3.11.9"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class IndustryAgnosticProductsServicesRecord extends UpdatableRecordImpl<IndustryAgnosticProductsServicesRecord> implements Record3<Integer, Integer, Integer> {

    private static final long serialVersionUID = 213533586;

    /**
     * Create a detached IndustryAgnosticProductsServicesRecord
     */
    public IndustryAgnosticProductsServicesRecord() {
        super(IndustryAgnosticProductsServices.INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES);
    }

    /**
     * Create a detached, initialised IndustryAgnosticProductsServicesRecord
     */
    public IndustryAgnosticProductsServicesRecord(Integer id, Integer positionId, Integer productServiceId) {
        super(IndustryAgnosticProductsServices.INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES);

        set(0, id);
        set(1, positionId);
        set(2, productServiceId);
    }

    /**
     * Getter for <code>ess_ontology.industry_agnostic_products_services.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>ess_ontology.industry_agnostic_products_services.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>ess_ontology.industry_agnostic_products_services.position_id</code>.
     */
    public Integer getPositionId() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>ess_ontology.industry_agnostic_products_services.position_id</code>.
     */
    public void setPositionId(Integer value) {
        set(1, value);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * Getter for <code>ess_ontology.industry_agnostic_products_services.product_service_id</code>.
     */
    public Integer getProductServiceId() {
        return (Integer) get(2);
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * Setter for <code>ess_ontology.industry_agnostic_products_services.product_service_id</code>.
     */
    public void setProductServiceId(Integer value) {
        set(2, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Integer, Integer, Integer> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Integer, Integer, Integer> valuesRow() {
        return (Row3) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return IndustryAgnosticProductsServices.INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field2() {
        return IndustryAgnosticProductsServices.INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES.POSITION_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return IndustryAgnosticProductsServices.INDUSTRY_AGNOSTIC_PRODUCTS_SERVICES.PRODUCT_SERVICE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component2() {
        return getPositionId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component3() {
        return getProductServiceId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value2() {
        return getPositionId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getProductServiceId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryAgnosticProductsServicesRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryAgnosticProductsServicesRecord value2(Integer value) {
        setPositionId(value);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryAgnosticProductsServicesRecord value3(Integer value) {
        setProductServiceId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndustryAgnosticProductsServicesRecord values(Integer value1, Integer value2, Integer value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }
}
