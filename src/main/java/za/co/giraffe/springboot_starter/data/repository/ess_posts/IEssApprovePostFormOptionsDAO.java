package za.co.giraffe.springboot_starter.data.repository.ess_posts;

import za.co.giraffe.springboot_starter.model.ess.Industry;

import java.util.List;

public interface IEssApprovePostFormOptionsDAO {
    List<Industry> fetchIndustries();
}
