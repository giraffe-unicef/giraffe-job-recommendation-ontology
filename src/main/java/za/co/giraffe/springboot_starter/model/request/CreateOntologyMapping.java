package za.co.giraffe.springboot_starter.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CreateOntologyMapping {

    @JsonProperty("position_id")
    private Integer positionId;
    @JsonProperty("industry_ids")
    private List<Integer> industryIds;
    @JsonProperty("section_ids")
    private List<Integer> inputIds;
    @JsonProperty("agnostic_industry")
    private boolean agnosticIndustry;
}