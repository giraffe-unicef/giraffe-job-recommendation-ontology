/**
 * NOTE: This class is auto generated by the swagger code generator program (3.0.8).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package za.co.giraffe.springboot_starter.api.ontology;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.Position;
import za.co.giraffe.springboot_starter.model.ess.Function;
import za.co.giraffe.springboot_starter.model.ess.Industry;
import za.co.giraffe.springboot_starter.model.ontology.ProductsAndServices;
import za.co.giraffe.springboot_starter.model.ontology.Qualification;
import za.co.giraffe.springboot_starter.model.ontology.Skill;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-05-16T13:33:34.418Z[GMT]")
@Api(value = "Ontology API", description = " ", tags = {"Ontology API"})
public interface OntologyApi {

    @ApiOperation(value = "Get all functions", nickname = "getAllFunctions", notes = "", response = Function.class,
            responseContainer = "List", tags = {"Ontology API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Function.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/functions",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Function>> getAllFunctions();

    @ApiOperation(value = "Get positions by function ID", nickname = "getPositionsByFunctionIds", notes = "",
            response = Position.class, responseContainer = "List", tags = {"Ontology API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Position.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/positions",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Position>> getPositionsByFunctionId
            (@NotNull @ApiParam(value = "", required = true) @Valid @RequestParam(value = "function_id", required = true) Integer functionId);

    @ApiOperation(value = "Get products and services by industry ID and position IDs", nickname = "getProductsAndServicesByIndustryAndPositionIds",
            notes = "", response = ProductsAndServices.class, responseContainer = "List", tags = {"Ontology API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = ProductsAndServices.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/products-and-services",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<ProductsAndServices>> getProductsAndServicesByIndustryAndPositionIds
            (@ApiParam(value = "Industry ID") @Valid @RequestParam(value = "industry_id", required = true) Integer industryId,
             @ApiParam(value = "One or more position IDs") @Valid @RequestParam(value = "position_ids", required = false) List<Integer> positionIds);

    @ApiOperation(value = "Get qualifications by industry ID and position IDs", nickname = "getQualificationsByIndustryAndPositionIds",
            notes = "", response = Qualification.class, responseContainer = "List", tags = {"Ontology API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Qualification.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/qualifications",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Qualification>> getQualificationsByIndustryAndPositionIds
            (@ApiParam(value = "Industry ID") @Valid @RequestParam(value = "industry_id", required = true) Integer industryId,
             @ApiParam(value = "One or more position IDs") @Valid @RequestParam(value = "position_ids", required = false) List<Integer> positionIds);

    @ApiOperation(value = "Get skills by industry ID and position IDs", nickname = "getSkillsByIndustryAndPositionIds",
            notes = "", response = Skill.class, responseContainer = "List", tags = {"Ontology API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Skill.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/skills",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Skill>> getSkillsByIndustryAndPositionIds
            (@ApiParam(value = "Industry ID") @Valid @RequestParam(value = "industry_id", required = true) Integer industryId,
             @ApiParam(value = "One or more position IDs") @Valid @RequestParam(value = "position_ids", required = false) List<Integer> positionIds);

    @ApiOperation(value = "Get all industries", nickname = "getAllIndustries", notes = "", response = Industry.class,
            responseContainer = "List", tags = {"Ontology API",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = Industry.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "resource not found", response = String.class),
            @ApiResponse(code = 500, message = "internal server error", response = String.class)})
    @RequestMapping(value = "/industries",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Industry>> getAllIndustries();
}
