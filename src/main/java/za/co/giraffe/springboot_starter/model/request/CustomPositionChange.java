package za.co.giraffe.springboot_starter.model.request;

import lombok.Data;

import java.util.UUID;

@Data
public class CustomPositionChange {
    private Integer id;
    private UUID customPositionId;
    private Integer experienceId;
    private String name;
    private boolean includeInQb = true;

    @Override
    public String toString() {
        return "CustomPositionChange{" +
                "id=" + id +
                ", customPositionId=" + customPositionId +
                ", experienceId=" + experienceId +
                ", includeInQb=" + includeInQb +
                ", name='" + name + '\'' +
                '}';
    }

}