/*
 * This file is generated by jOOQ.
 */
package za.co.giraffe.infrastructure.entities.jooq.tables.pojos;


import javax.annotation.Generated;
import java.io.Serializable;
import java.util.Arrays;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.jooq.org",
                "jOOQ version:3.11.9"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class IndustryPositionProductsServicesGroup implements Serializable {

    private static final long serialVersionUID = 1342393385;

    private Integer[] positionIds;
    private Integer[] industryIds;
    private Integer[] productServiceIds;
    private Boolean agnosticProductsServices;

    public IndustryPositionProductsServicesGroup() {
    }

    public IndustryPositionProductsServicesGroup(IndustryPositionProductsServicesGroup value) {
        this.positionIds = value.positionIds;
        this.industryIds = value.industryIds;
        this.productServiceIds = value.productServiceIds;
        this.agnosticProductsServices = value.agnosticProductsServices;
    }

    public IndustryPositionProductsServicesGroup(
            Integer[] positionIds,
            Integer[] industryIds,
            Integer[] productServiceIds,
            Boolean agnosticProductsServices
    ) {
        this.positionIds = positionIds;
        this.industryIds = industryIds;
        this.productServiceIds = productServiceIds;
        this.agnosticProductsServices = agnosticProductsServices;
    }

    public Integer[] getPositionIds() {
        return this.positionIds;
    }

    public void setPositionIds(Integer... positionIds) {
        this.positionIds = positionIds;
    }

    public Integer[] getIndustryIds() {
        return this.industryIds;
    }

    public void setIndustryIds(Integer... industryIds) {
        this.industryIds = industryIds;
    }

    public Integer[] getProductServiceIds() {
        return this.productServiceIds;
    }

    public void setProductServiceIds(Integer... productServiceIds) {
        this.productServiceIds = productServiceIds;
    }

    public Boolean getAgnosticProductsServices() {
        return this.agnosticProductsServices;
    }

    public void setAgnosticProductsServices(Boolean agnosticProductsServices) {
        this.agnosticProductsServices = agnosticProductsServices;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("IndustryPositionProductsServicesGroup (");

        sb.append(Arrays.toString(positionIds));
        sb.append(", ").append(Arrays.toString(industryIds));
        sb.append(", ").append(Arrays.toString(productServiceIds));
        sb.append(", ").append(agnosticProductsServices);

        sb.append(")");
        return sb.toString();
    }
}
