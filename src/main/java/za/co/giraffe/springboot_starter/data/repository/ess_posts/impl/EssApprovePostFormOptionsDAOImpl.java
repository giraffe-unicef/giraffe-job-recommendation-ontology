package za.co.giraffe.springboot_starter.data.repository.ess_posts.impl;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.giraffe.springboot_starter.data.repository.ess_posts.IEssApprovePostFormOptionsDAO;
import za.co.giraffe.springboot_starter.model.ess.Industry;

import java.util.List;

import static za.co.giraffe.infrastructure.entities.jooq.Tables.INDUSTRY;

@Service
@Transactional("transactionManager")
public class EssApprovePostFormOptionsDAOImpl implements IEssApprovePostFormOptionsDAO {
    private DSLContext dsl;

    @Autowired
    public EssApprovePostFormOptionsDAOImpl(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public List<Industry> fetchIndustries() {
        List<Industry> records = null;
        try {
            records = dsl.select().from(INDUSTRY).fetchInto(Industry.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return records;
    }
}
