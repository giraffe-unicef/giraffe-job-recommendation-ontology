package za.co.giraffe.infrastructure.database;

import org.jooq.DSLContext;

public interface JooqDatabaseInf extends DatabaseInf {
    void withTransaction(DSLContext context);
}
