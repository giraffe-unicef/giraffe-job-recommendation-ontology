package za.co.giraffe.springboot_starter.services.ontology;

import za.co.giraffe.infrastructure.entities.jooq.tables.pojos.*;
import za.co.giraffe.springboot_starter.model.ess.Function;
import za.co.giraffe.springboot_starter.model.ess.Industry;
import za.co.giraffe.springboot_starter.model.ontology.Qualification;
import za.co.giraffe.springboot_starter.model.ontology.Skill;
import za.co.giraffe.springboot_starter.model.ontology.*;
import za.co.giraffe.springboot_starter.model.request.*;

import java.util.List;

public interface IOntologyService {
    List<Function> handleFetchFunctions();

    List<Position> handleFetchPositionsByFunctionId(Integer id);

    List<ProductsAndServices> handleFetchProductsAndServicesByIndustryPositionIds(Integer industryId, List<Integer> positionIds);

    List<Qualification> handleFetchQualificationsByIndustryPositionIds(Integer industryId, List<Integer> positionIds);

    List<Skill> handleFetchSkillsByIndustryPositionIds(Integer industryId, List<Integer> positionIds);

    List<Industry> handleFetchIndustries();

    /**
     * Ontology Editor Service
     */

    List<PositionWithAgnosticMapping> handleFetchAllPosition(Boolean agnosticIndustry, String section);

    List<ProductsAndServices> handleFetchAllProductsAndServices();

    List<Qualification> handleFetchAllQualifications();

    List<Skill> handleFetchAllSkills();

    PositionWithAgnosticMapping handleCreatePositionWithAgnosticMapping(PositionRequest positionRequest);

    void handleUpsertProductsAndServices(Integer id, OntologyEditorRequest ontologyEditorRequest);

    void handleUpsertQualifications(Integer id, OntologyEditorRequest ontologyEditorRequest);

    void handleUpsertSkills(Integer id, OntologyEditorRequest ontologyEditorRequest);

    void handleUpdatePosition(Integer id, OntologyEditorRequest ontologyEditorRequest);

    List<PositionFunctionMapping> handleFetchAllPositionFunctionMapping();

    void handleUpsertPositionFunctionMapping(UpsertPositionFunctionMapping upsertPositionFunctionMapping);

    List<OntologyMaster> handleFetchAllOntologyMapping();
    /*void handleUpsertOntologyMaster(UpdateOntologyMasterMapping updateOntologyMasterMapping) throws Exception;*/

    List<IndustryPositionProductsServicesGroup> handleFetchIndustryPositionProductsServicesGroup();

    List<IndustryPositionQualificationsGroup> handleFetchIndustryPositionQualificationsGroup();

    List<IndustryPositionSkillsGroup> handleFetchIndustryPositionSkillsGroup();

    void handleUpsertIndustryPositionProductsServicesGroupMapping(UpdateOntologyMapping updateOntologyMasterMapping) throws Exception;

    void handleUpsertIndustryPositionQualificationsGroupMapping(UpdateOntologyMapping updateOntologyMasterMapping) throws Exception;

    void handleUpsertIndustryPositionSkillsGroupMapping(UpdateOntologyMapping updateOntologyMasterMapping) throws Exception;

    void handleCreateIndustryPositionProductAndServiceGroupMapping(CreateOntologyMapping createOntologyMapping) throws Exception;

    void handleCreateIndustryPositionQualificationMapping(CreateOntologyMapping createOntologyMapping) throws Exception;

    void handleCreateIndustryPositionSkillsGroupMapping(CreateOntologyMapping createOntologyMapping) throws Exception;
}
