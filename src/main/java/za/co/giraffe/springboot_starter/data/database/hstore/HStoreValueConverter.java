package za.co.giraffe.springboot_starter.data.database.hstore;

import org.jooq.Converter;
import org.postgresql.util.HStoreConverter;
import za.co.giraffe.infrastructure.database.hstore.HStoreValue;

public final class HStoreValueConverter implements Converter<Object, HStoreValue> {

    private static final long serialVersionUID = 1L;

    @Override
    public final HStoreValue from(Object databaseObject) {
        if (databaseObject == null) {
            return null;
        } else {
            return HStoreValue.parse(databaseObject.toString());
        }
    }

    @Override
    public final Object to(HStoreValue userObject) {
        if (userObject == null) {
            return null;
        } else {
            return HStoreConverter.toString(userObject);
        }
    }

    @Override
    public final Class<Object> fromType() {
        return Object.class;
    }

    @Override
    public final Class<HStoreValue> toType() {
        return HStoreValue.class;
    }
}